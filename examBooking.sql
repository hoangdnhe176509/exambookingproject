USE [master]
GO
/****** Object:  Database [examBooking]    Script Date: 10-May-24 11:03:03 PM ******/
CREATE DATABASE [examBooking]
 CONTAINMENT = NONE
 ON  PRIMARY 
( NAME = N'examBooking', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL16.SQLEXPRESS\MSSQL\DATA\examBooking.mdf' , SIZE = 8192KB , MAXSIZE = UNLIMITED, FILEGROWTH = 65536KB )
 LOG ON 
( NAME = N'examBooking_log', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL16.SQLEXPRESS\MSSQL\DATA\examBooking_log.ldf' , SIZE = 8192KB , MAXSIZE = 2048GB , FILEGROWTH = 65536KB )
 WITH CATALOG_COLLATION = DATABASE_DEFAULT, LEDGER = OFF
GO
ALTER DATABASE [examBooking] SET COMPATIBILITY_LEVEL = 160
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [examBooking].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO
ALTER DATABASE [examBooking] SET ANSI_NULL_DEFAULT OFF 
GO
ALTER DATABASE [examBooking] SET ANSI_NULLS OFF 
GO
ALTER DATABASE [examBooking] SET ANSI_PADDING OFF 
GO
ALTER DATABASE [examBooking] SET ANSI_WARNINGS OFF 
GO
ALTER DATABASE [examBooking] SET ARITHABORT OFF 
GO
ALTER DATABASE [examBooking] SET AUTO_CLOSE ON 
GO
ALTER DATABASE [examBooking] SET AUTO_SHRINK OFF 
GO
ALTER DATABASE [examBooking] SET AUTO_UPDATE_STATISTICS ON 
GO
ALTER DATABASE [examBooking] SET CURSOR_CLOSE_ON_COMMIT OFF 
GO
ALTER DATABASE [examBooking] SET CURSOR_DEFAULT  GLOBAL 
GO
ALTER DATABASE [examBooking] SET CONCAT_NULL_YIELDS_NULL OFF 
GO
ALTER DATABASE [examBooking] SET NUMERIC_ROUNDABORT OFF 
GO
ALTER DATABASE [examBooking] SET QUOTED_IDENTIFIER OFF 
GO
ALTER DATABASE [examBooking] SET RECURSIVE_TRIGGERS OFF 
GO
ALTER DATABASE [examBooking] SET  ENABLE_BROKER 
GO
ALTER DATABASE [examBooking] SET AUTO_UPDATE_STATISTICS_ASYNC OFF 
GO
ALTER DATABASE [examBooking] SET DATE_CORRELATION_OPTIMIZATION OFF 
GO
ALTER DATABASE [examBooking] SET TRUSTWORTHY OFF 
GO
ALTER DATABASE [examBooking] SET ALLOW_SNAPSHOT_ISOLATION OFF 
GO
ALTER DATABASE [examBooking] SET PARAMETERIZATION SIMPLE 
GO
ALTER DATABASE [examBooking] SET READ_COMMITTED_SNAPSHOT OFF 
GO
ALTER DATABASE [examBooking] SET HONOR_BROKER_PRIORITY OFF 
GO
ALTER DATABASE [examBooking] SET RECOVERY SIMPLE 
GO
ALTER DATABASE [examBooking] SET  MULTI_USER 
GO
ALTER DATABASE [examBooking] SET PAGE_VERIFY CHECKSUM  
GO
ALTER DATABASE [examBooking] SET DB_CHAINING OFF 
GO
ALTER DATABASE [examBooking] SET FILESTREAM( NON_TRANSACTED_ACCESS = OFF ) 
GO
ALTER DATABASE [examBooking] SET TARGET_RECOVERY_TIME = 60 SECONDS 
GO
ALTER DATABASE [examBooking] SET DELAYED_DURABILITY = DISABLED 
GO
ALTER DATABASE [examBooking] SET ACCELERATED_DATABASE_RECOVERY = OFF  
GO
ALTER DATABASE [examBooking] SET QUERY_STORE = ON
GO
ALTER DATABASE [examBooking] SET QUERY_STORE (OPERATION_MODE = READ_WRITE, CLEANUP_POLICY = (STALE_QUERY_THRESHOLD_DAYS = 30), DATA_FLUSH_INTERVAL_SECONDS = 900, INTERVAL_LENGTH_MINUTES = 60, MAX_STORAGE_SIZE_MB = 1000, QUERY_CAPTURE_MODE = AUTO, SIZE_BASED_CLEANUP_MODE = AUTO, MAX_PLANS_PER_QUERY = 200, WAIT_STATS_CAPTURE_MODE = ON)
GO
USE [examBooking]
GO
/****** Object:  Table [dbo].[BookingRequests]    Script Date: 10-May-24 11:03:03 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[BookingRequests](
	[RequestID] [int] IDENTITY(1,1) NOT NULL,
	[UserID] [int] NULL,
	[ExamScheduleID] [int] NULL,
	[RequestDate] [date] NULL,
	[Status] [varchar](20) NULL,
PRIMARY KEY CLUSTERED 
(
	[RequestID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Exams]    Script Date: 10-May-24 11:03:04 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Exams](
	[ExamID] [int] IDENTITY(1,1) NOT NULL,
	[ExamName] [varchar](100) NULL,
	[Subject] [varchar](50) NULL,
PRIMARY KEY CLUSTERED 
(
	[ExamID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[ExamSchedule]    Script Date: 10-May-24 11:03:04 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ExamSchedule](
	[ScheduleID] [int] IDENTITY(1,1) NOT NULL,
	[UserID] [int] NULL,
	[ExamID] [int] NULL,
	[RoomID] [int] NULL,
	[SlotID] [int] NULL,
	[date] [date] NULL,
PRIMARY KEY CLUSTERED 
(
	[ScheduleID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Rooms]    Script Date: 10-May-24 11:03:04 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Rooms](
	[RoomID] [int] IDENTITY(1,1) NOT NULL,
	[RoomName] [varchar](50) NULL,
PRIMARY KEY CLUSTERED 
(
	[RoomID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Slots]    Script Date: 10-May-24 11:03:04 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Slots](
	[SlotID] [int] IDENTITY(1,1) NOT NULL,
	[SLotName] [nvarchar](50) NULL,
	[StartTime] [time](7) NULL,
	[EndTime] [time](7) NULL,
PRIMARY KEY CLUSTERED 
(
	[SlotID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Users]    Script Date: 10-May-24 11:03:04 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Users](
	[UserID] [int] IDENTITY(1,1) NOT NULL,
	[FirstName] [varchar](50) NULL,
	[LastName] [varchar](50) NULL,
	[Email] [varchar](100) NULL,
	[Password] [varchar](50) NULL,
	[Role] [varchar](20) NULL,
PRIMARY KEY CLUSTERED 
(
	[UserID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
SET IDENTITY_INSERT [dbo].[BookingRequests] ON 

INSERT [dbo].[BookingRequests] ([RequestID], [UserID], [ExamScheduleID], [RequestDate], [Status]) VALUES (28, 4, 27, CAST(N'2024-03-06' AS Date), N'Cancel')
INSERT [dbo].[BookingRequests] ([RequestID], [UserID], [ExamScheduleID], [RequestDate], [Status]) VALUES (29, 4, 29, CAST(N'2024-03-06' AS Date), N'Approve')
INSERT [dbo].[BookingRequests] ([RequestID], [UserID], [ExamScheduleID], [RequestDate], [Status]) VALUES (32, 6, 27, CAST(N'2024-03-06' AS Date), N'Approve')
INSERT [dbo].[BookingRequests] ([RequestID], [UserID], [ExamScheduleID], [RequestDate], [Status]) VALUES (36, 6, 35, CAST(N'2024-03-13' AS Date), N'Approve')
INSERT [dbo].[BookingRequests] ([RequestID], [UserID], [ExamScheduleID], [RequestDate], [Status]) VALUES (37, 7, 37, CAST(N'2024-03-13' AS Date), N'Approve')
INSERT [dbo].[BookingRequests] ([RequestID], [UserID], [ExamScheduleID], [RequestDate], [Status]) VALUES (39, 7, 40, CAST(N'2024-03-13' AS Date), N'Cancel')
INSERT [dbo].[BookingRequests] ([RequestID], [UserID], [ExamScheduleID], [RequestDate], [Status]) VALUES (49, 6, 40, CAST(N'2024-03-13' AS Date), N'Approve')
SET IDENTITY_INSERT [dbo].[BookingRequests] OFF
GO
SET IDENTITY_INSERT [dbo].[Exams] ON 

INSERT [dbo].[Exams] ([ExamID], [ExamName], [Subject]) VALUES (12, N'FE DBI202', N'Database')
INSERT [dbo].[Exams] ([ExamID], [ExamName], [Subject]) VALUES (13, N'Midtern JPD123', N'Elementary Japanese')
INSERT [dbo].[Exams] ([ExamID], [ExamName], [Subject]) VALUES (14, N'FE JPD113', N'Elementary Japanese')
INSERT [dbo].[Exams] ([ExamID], [ExamName], [Subject]) VALUES (15, N'FE MAS291', N'Statistics and Probability')
INSERT [dbo].[Exams] ([ExamID], [ExamName], [Subject]) VALUES (16, N'PRC392c', N'Cloud Computing')
INSERT [dbo].[Exams] ([ExamID], [ExamName], [Subject]) VALUES (49, N'PE DBI202', N'123')
INSERT [dbo].[Exams] ([ExamID], [ExamName], [Subject]) VALUES (50, N'PE_PRM', N'C#')
INSERT [dbo].[Exams] ([ExamID], [ExamName], [Subject]) VALUES (52, N'DBI202', N'Database')
SET IDENTITY_INSERT [dbo].[Exams] OFF
GO
SET IDENTITY_INSERT [dbo].[ExamSchedule] ON 

INSERT [dbo].[ExamSchedule] ([ScheduleID], [UserID], [ExamID], [RoomID], [SlotID], [date]) VALUES (27, 6, 15, 3, 5, CAST(N'2024-03-08' AS Date))
INSERT [dbo].[ExamSchedule] ([ScheduleID], [UserID], [ExamID], [RoomID], [SlotID], [date]) VALUES (29, 4, 12, 4, 6, CAST(N'2024-03-08' AS Date))
INSERT [dbo].[ExamSchedule] ([ScheduleID], [UserID], [ExamID], [RoomID], [SlotID], [date]) VALUES (35, 6, 12, 3, 2, CAST(N'2024-03-13' AS Date))
INSERT [dbo].[ExamSchedule] ([ScheduleID], [UserID], [ExamID], [RoomID], [SlotID], [date]) VALUES (37, 7, 12, 4, 4, CAST(N'2024-03-14' AS Date))
INSERT [dbo].[ExamSchedule] ([ScheduleID], [UserID], [ExamID], [RoomID], [SlotID], [date]) VALUES (40, 6, 15, 3, 1, CAST(N'2024-03-13' AS Date))
SET IDENTITY_INSERT [dbo].[ExamSchedule] OFF
GO
SET IDENTITY_INSERT [dbo].[Rooms] ON 

INSERT [dbo].[Rooms] ([RoomID], [RoomName]) VALUES (1, N'DE101')
INSERT [dbo].[Rooms] ([RoomID], [RoomName]) VALUES (2, N'DE220')
INSERT [dbo].[Rooms] ([RoomID], [RoomName]) VALUES (3, N'BE303')
INSERT [dbo].[Rooms] ([RoomID], [RoomName]) VALUES (4, N'BE202')
SET IDENTITY_INSERT [dbo].[Rooms] OFF
GO
SET IDENTITY_INSERT [dbo].[Slots] ON 

INSERT [dbo].[Slots] ([SlotID], [SLotName], [StartTime], [EndTime]) VALUES (1, N'Slot 1 ', CAST(N'07:30:00' AS Time), CAST(N'09:10:00' AS Time))
INSERT [dbo].[Slots] ([SlotID], [SLotName], [StartTime], [EndTime]) VALUES (2, N'Slot 2 ', CAST(N'09:10:00' AS Time), CAST(N'10:20:00' AS Time))
INSERT [dbo].[Slots] ([SlotID], [SLotName], [StartTime], [EndTime]) VALUES (3, N'Slot 3 ', CAST(N'10:30:00' AS Time), CAST(N'12:10:00' AS Time))
INSERT [dbo].[Slots] ([SlotID], [SLotName], [StartTime], [EndTime]) VALUES (4, N'Slot 4 ', CAST(N'12:50:00' AS Time), CAST(N'14:20:00' AS Time))
INSERT [dbo].[Slots] ([SlotID], [SLotName], [StartTime], [EndTime]) VALUES (5, N'Slot 5 ', CAST(N'14:30:00' AS Time), CAST(N'16:00:00' AS Time))
INSERT [dbo].[Slots] ([SlotID], [SLotName], [StartTime], [EndTime]) VALUES (6, N'Slot 6 ', CAST(N'16:10:00' AS Time), CAST(N'17:40:00' AS Time))
SET IDENTITY_INSERT [dbo].[Slots] OFF
GO
SET IDENTITY_INSERT [dbo].[Users] ON 

INSERT [dbo].[Users] ([UserID], [FirstName], [LastName], [Email], [Password], [Role]) VALUES (1, N'Super', N'Admin', N'admin@gmail.com', N'123', N'admin')
INSERT [dbo].[Users] ([UserID], [FirstName], [LastName], [Email], [Password], [Role]) VALUES (2, N'Teacher', N'tuan', N'teacher@gmail.com', N'123', N'teacher')
INSERT [dbo].[Users] ([UserID], [FirstName], [LastName], [Email], [Password], [Role]) VALUES (3, N'Teacher', N'dat', N'teacher2@gmail.com', N'123', N'teacher')
INSERT [dbo].[Users] ([UserID], [FirstName], [LastName], [Email], [Password], [Role]) VALUES (4, N'Nhat', N'Hoang', N'slhoang2003@gmail.com', N'123', N'teacher')
INSERT [dbo].[Users] ([UserID], [FirstName], [LastName], [Email], [Password], [Role]) VALUES (5, N'van ', N'hoa', N'hoa123@gmail.com', N'123', N'teacher')
INSERT [dbo].[Users] ([UserID], [FirstName], [LastName], [Email], [Password], [Role]) VALUES (6, N'Lee', N'Hanh', N'hanh123@gmail.com', N'123', N'teacher')
INSERT [dbo].[Users] ([UserID], [FirstName], [LastName], [Email], [Password], [Role]) VALUES (7, N'thanh', N'dang', N'dang123@gmail.com', N'123', N'teacher')
INSERT [dbo].[Users] ([UserID], [FirstName], [LastName], [Email], [Password], [Role]) VALUES (8, N'n', N'h', N'nh123@gmail.com', N'123', N'teacher')
INSERT [dbo].[Users] ([UserID], [FirstName], [LastName], [Email], [Password], [Role]) VALUES (9, N'kiu ', N'truc', N'trucbeo@gmail.com', N'123', N'teacher')
SET IDENTITY_INSERT [dbo].[Users] OFF
GO
ALTER TABLE [dbo].[BookingRequests] ADD  DEFAULT ('Pending') FOR [Status]
GO
ALTER TABLE [dbo].[BookingRequests]  WITH CHECK ADD  CONSTRAINT [FK__BookingRe__ExamS__46E78A0C] FOREIGN KEY([ExamScheduleID])
REFERENCES [dbo].[ExamSchedule] ([ScheduleID])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[BookingRequests] CHECK CONSTRAINT [FK__BookingRe__ExamS__46E78A0C]
GO
ALTER TABLE [dbo].[BookingRequests]  WITH CHECK ADD FOREIGN KEY([UserID])
REFERENCES [dbo].[Users] ([UserID])
GO
ALTER TABLE [dbo].[ExamSchedule]  WITH CHECK ADD FOREIGN KEY([ExamID])
REFERENCES [dbo].[Exams] ([ExamID])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[ExamSchedule]  WITH CHECK ADD FOREIGN KEY([RoomID])
REFERENCES [dbo].[Rooms] ([RoomID])
GO
ALTER TABLE [dbo].[ExamSchedule]  WITH CHECK ADD FOREIGN KEY([SlotID])
REFERENCES [dbo].[Slots] ([SlotID])
GO
ALTER TABLE [dbo].[ExamSchedule]  WITH CHECK ADD FOREIGN KEY([UserID])
REFERENCES [dbo].[Users] ([UserID])
GO
USE [master]
GO
ALTER DATABASE [examBooking] SET  READ_WRITE 
GO
