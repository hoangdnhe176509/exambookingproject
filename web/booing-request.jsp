
<%@ page language="java" contentType="text/html; charset=UTF-8"%>
<%@ include file="./view/header.jsp"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<main class="container py-5">

    <h1>Booking Management</h1>
    <c:if test="${not empty param.msg}">
        <div class="alert alert-success" role="alert">
            ${param.msg}
        </div>
    </c:if>

    <!-- Check if there is an error message -->
    <c:if test="${not empty param.err}">
        <div class="alert alert-danger" role="alert">
            ${param.err}
        </div>
    </c:if>
    <c:if test="${account.role eq 'teacher'}">
        <table class="table">
            <thead>
                <tr>
                    <th>Exam Type</th>
                    <th>Room</th>
                    <th>Slot</th>
                    <th>Date</th>
                    <th>Status</th>
                    <th>Action</th>
                </tr>
            </thead>
            <tbody>
                
                <c:forEach var="schedule" items="${pagedBookingRequests}">
                    <tr>
                        <td>${schedule.examName}</td>
                        <td>${schedule.roomName}</td>
                        <td>${schedule.slotName}</td>
                        <td>${schedule.examDate}</td>
                        <td style="color:
                            <c:choose>
                                <c:when test="${schedule.status eq 'Cancel'}">red</c:when>
                                <c:when test="${schedule.status eq 'Approved'}">green</c:when>
                                <c:when test="${schedule.status eq 'Pending'}">yellow</c:when>
                                <c:otherwise>black</c:otherwise>
                            </c:choose>
                            ">${schedule.status}</td>
                        <td>
                            <form action="teacher-request" method="post" onsubmit="return confirm('Are you sure you want to cancel this request?')">
                                <input type="hidden" name="bookingId" value="${schedule.id}" />
                                <input type="hidden" name="action" value="cancel" />
                                <button type="submit" class="btn btn-danger" ${schedule.status eq 'Cancel' or schedule.status eq 'Approve' ? 'disabled' : ''}>Cancel</button>
                            </form>
                        </td>
                    </tr>
                </c:forEach>
                    
            </tbody>
        </table>
        <nav class="mt-3" aria-label="Page navigation example">
            <ul class="pagination justify-content-center">
                <c:if test="${currentPage > 1}">
                    <li class="page-item">
                        <a class="page-link" href="?page=${currentPage - 1}">Previous</a>
                    </li>
                </c:if>

                <c:forEach var="pageNum" begin="1" end="${totalPages}">
                    <li class="page-item <c:if test='${pageNum == currentPage}'>active</c:if>">
                        <a class="page-link" href="?page=${pageNum}">${pageNum}</a>
                    </li>
                </c:forEach>

                <c:if test="${currentPage < totalPages}">
                    <li class="page-item">
                        <a class="page-link" href="?;page=${currentPage + 1}">Next</a>
                    </li>
                </c:if>
            </ul>
        </nav>
    </c:if>
    <c:if test="${account.role eq 'admin'}">
        <table class="table">
            <thead>
                <tr>
                    <th>Exam Type</th>
                    <th>Room</th>
                    <th>Slot</th>
                    <th>Teacher</th>
                    <th>Date</th>
                    <th>Status</th>
                    <th>Action</th>
                </tr>
            </thead>
            <tbody>
                <c:forEach var="schedule" items="${pagedBookingRequests}">
                    <tr>
                        <td>${schedule.examName}</td>
                        <td>${schedule.roomName}</td>
                        <td>${schedule.slotName}</td>
                        <td>${schedule.userName}</td>
                        <td>${schedule.examDate}</td>
                        <td style="color:
                            <c:choose>
                                <c:when test="${schedule.status eq 'Cancel'}">red</c:when>
                                <c:when test="${schedule.status eq 'Approved'}">green</c:when>
                                <c:when test="${schedule.status eq 'Pending'}">yellow</c:when>
                                <c:otherwise>black</c:otherwise>
                            </c:choose>
                            ">${schedule.status}</td>
                        <td>
                            <form action="teacher-request" method="post" onsubmit="return confirm('Are you sure you want to Approve this request?')">
                                <input type="hidden" name="bookingId" value="${schedule.id}" />
                                <input type="hidden" name="action" value="approve" />
                                <button type="submit" class="btn btn-success" ${schedule.status eq 'Cancel' or schedule.status eq'Approve' ? 'disabled' : ''}>Approve</button>
                            </form>
                        </td>
                    </tr>
                </c:forEach>
            </tbody>
        </table>
        <nav class="mt-3" aria-label="Page navigation example">
            <ul class="pagination justify-content-center">
                <c:if test="${currentPage > 1}">
                    <li class="page-item">
                        <a class="page-link" href="?page=${currentPage - 1}">Previous</a>
                    </li>
                </c:if>

                <c:forEach var="pageNum" begin="1" end="${totalPages}">
                    <li class="page-item <c:if test='${pageNum == currentPage}'>active</c:if>">
                        <a class="page-link" href="?page=${pageNum}">${pageNum}</a>
                    </li>
                </c:forEach>

                <c:if test="${currentPage < totalPages}">
                    <li class="page-item">
                        <a class="page-link" href="?page=${currentPage + 1}">Next</a>
                    </li>
                </c:if>
            </ul>
        </nav>
    </c:if>
    <!-- paging -->


</main>

<script src="https://code.jquery.com/jquery-3.6.0.min.js"></script>

<!-- Include Bootstrap JS and Popper.js -->
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-YvpcrYf0tY3lHB60NNkmXc5s9fDVZLESaAA55NDzOxhy9GkcIdslK1eN7N6jIeHz" crossorigin="anonymous"></script>
<%@ include file="./view/footer.jsp"%>

