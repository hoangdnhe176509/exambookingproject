<%@page import="java.time.format.DateTimeFormatter"%>
<%@page import="java.util.List"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page import="java.time.LocalDate"%>
<%@page import="java.time.DayOfWeek"%>
<%@page import="java.time.format.DateTimeFormatter"%>
<%@page import="java.time.temporal.TemporalAdjusters"%>
<!DOCTYPE html>
<html lang="en">

    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <title>Exam Booking System</title>
        <!-- Bootstrap CSS -->
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.3/css/all.min.css">
        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.3/dist/css/bootstrap.min.css" rel="stylesheet"
              integrity="sha384-QWTKZyjpPEjISv5WaRU9OFeRpok6YctnYmDr5pNlyT2bRjXh0JMhjY6hW+ALEwIH" crossorigin="anonymous">
        <style>
            body {
                min-height: 100vh;
                display: flex;
                flex-direction: column;
            }

            main {
                flex: 1;
            }
            table {
                border-collapse: collapse;
                width: 100%;
                margin-top: 20px; /* Added margin for spacing from the header */
            }
            th, td {
                border: 1px solid #dddddd;
                padding: 8px;
                text-align: center;
            }
            td p {
                font-size: 14px;
                font-weight: 600;
                text-align: left;
            }
            th {
                background-color: #0d6efd; /* Changed for better visibility */
                color: white; /* Added text color for contrast */
                font-weight: bold;
            }
            tr:nth-child(even) {
                background-color: #f2f2f2;
            }
            tr:hover {
                background-color: #dddddd;
            }
            h2 {
                text-align: center; /* Centering the title for a better look */
                margin-bottom: 20px; /* Added space between title and table */
            }
        </style>
    </head>

    <body class="bg-light d-flex flex-column">
        <header>
            <!-- Navigation bar for teacher -->
            <nav class="navbar navbar-expand-lg navbar-dark bg-primary">
                <div class="container-fluid">

                    <a class="navbar-brand" href="home.jsp">Exam Booing</a>
                    <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarNav"
                            aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
                        <span class="navbar-toggler-icon"></span>
                    </button>

                    <div class="collapse navbar-collapse justify-content-end" id="navbarNav">
                        <ul class="navbar-nav">

                            <li class="nav-item">
                                <a class="nav-link" href="home.jsp">Home</a>

                            </li>
                            <c:if test="${account.role eq 'admin'}">
                                <li class="nav-item">
                                    <a class="nav-link" href="exam-management">Exam Management</a>

                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" href="schedule-management">Schedule Management</a>

                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" href="teacher-request">Request Management</a>

                                </li>
                                <li class="nav-item">
                                    <%
             LocalDate today = LocalDate.now();
             LocalDate monday = today.with(TemporalAdjusters.previousOrSame(DayOfWeek.MONDAY));
             String startDate = monday.format(DateTimeFormatter.ofPattern("yyyy-MM-dd"));
                                    %>
                                    <a class="nav-link" href="${pageContext.request.contextPath}/view-schedule?startDate=<%=startDate%>">View Exam Schedule</a>

                                </li>

                            </c:if>
                            <li class="nav-item">
                                <a class="nav-link" href="logout">Logout</a>
                            </li>
                        </ul>
                    </div>
                </div>
            </nav>
        </header>
        <form id="weekSelectForm" action="${pageContext.request.contextPath}/view-schedule" method="get">
            <label for="startDate">Select Week Starting:</label>
            <select name="startDate" id="startDate">
            </select>
            <button type="submit">Show Schedule</button> <%-- Manual submission button --%>
        </form>

        <h2>Schedule for Week ${startDate.format(DateTimeFormatter.ofPattern("dd/MM"))} to ${endDate.format(DateTimeFormatter.ofPattern("dd/MM"))}</h2>
        <table>
            <thead>
                <tr>
                    <th>Date</th>
                    <th>Mon</th>
                    <th>Tue</th>
                    <th>Wed</th>
                    <th>Thu</th>
                    <th>Fri</th>
                    <th>Sat</th>
                    <th>Sun</th>
                </tr>
                <tr>
                    <th>No</th>
                        <c:forEach begin="1" end="7" var="day">
                        <th>${startDate.plusDays(day - 1).format(DateTimeFormatter.ofPattern("dd/MM"))}</th>
                        </c:forEach>
                </tr>
            </thead>
            <tbody>
                <c:forEach items="${schedule}" var="slot" varStatus="status">
                    <tr>
                       <td>${status.index + 1}</td>
                        <c:forEach begin="1" end="7" var="day">
                            <td>
                                <c:if test="${slot.date == startDate.plusDays(day - 1)}">
                                    <c:if test="${not empty slot and slot.date == startDate.plusDays(day - 1)}">
                                        <p>Subject: ${not empty slot.subject ? slot.subject : 'Not Assigned'}</p>
                                        <p>Time: ${not empty slot.startTime ? slot.startTime : 'Not Assigned'} - ${not empty slot.endTime ? slot.endTime : 'Not Assigned'}</p>
                                        <p>Room: ${not empty slot.roomName ? slot.roomName : 'Not Assigned'}</p>
                                        <p>Slot: <span style="color: red">${slot.slotName}</span> </p>
                                        <p>Exam type: ${not empty slot.examName ? slot.examName : 'Not Assigned'}</p>
                                        <p>Supervisor: ${not empty slot.firstName ? slot.firstName : 'Not Assigned'} ${not empty slot.lastName ? slot.lastName : ''}</p>
                                    </c:if>

                                </c:if>
                            </td>
                        </c:forEach>
                    </tr>
                </c:forEach>


            </tbody>
        </table>
    </body>
    <script>
        window.onload = function () {
            var startDateSelect = document.getElementById('startDate');
            var currentDate = new Date();
            var currentDay = currentDate.getDay();
            var distance = (currentDay == 0 ? -6 : 1) - currentDay; // Calculate distance to the previous Monday
            var monday = new Date(currentDate.setDate(currentDate.getDate() + distance));

            for (let i = 0; i < 52; i++) { // Generate options for 52 weeks
                let optionDate = new Date(monday.getTime() + (i * 7 * 24 * 60 * 60 * 1000));
                let optionValue = optionDate.toISOString().split('T')[0];
                let optionText = optionDate.toLocaleDateString('en-GB', {year: 'numeric', month: '2-digit', day: '2-digit'});

                let option = new Option(optionText, optionValue, i === 0, i === 0);
                startDateSelect.add(option);
            }
        };
    </script>
    <footer class="bg-primary text-white text-center py-4 mt-auto">
        <!-- Footer content -->
        <p>&copy; 2024 Exam Booing System. All rights reserved.</p>
    </footer>

    <!-- Bootstrap JS and dependencies -->
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.3/dist/js/bootstrap.bundle.min.js"
            integrity="sha384-YvpcrYf0tY3lHB60NNkmXc5s9fDVZLESaAA55NDzOxhy9GkcIdslK1eN7N6jIeHz"
    crossorigin="anonymous"></script>

</html>

