
<%@ page language="java" contentType="text/html; charset=UTF-8"%>
<%@ include file="./view/header.jsp"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<main class="container py-5">
    <div class="row mb-12">
        <div class="col-md-6">
            <div class="form-group">
                <form action="schedule-management" method="get">
                    <label for="searchInput" class="form-label">Search:</label>
                    <div class="input-group" style="width: 80%">
                        <input style="width: 80%" type="text" class="form-control" name="search" placeholder="Enter Exam Name or Subject to search!" value="${param.search}">
                        <input type="hidden" name="page" value="${currentPage}">

                    </div>
                </form>
            </div>
        </div>

    </div>
    <h1>Booking</h1>
    <c:if test="${not empty param.msg}">
        <div class="alert alert-success" role="alert">
            ${param.msg}
        </div>
    </c:if>

    <!-- Check if there is an error message -->
    <c:if test="${not empty param.err}">
        <div class="alert alert-danger" role="alert">
            ${param.err}
        </div>
    </c:if>

    <table class="table">
        <thead>
            <tr>
                <th>Exam Type</th>
                <th>Subject</th>
                <th>Room</th>
                <th>Slot</th>
                <th>Date</th>
                <th>Supervisor</th>
                <th>Action</th>
            </tr>
        </thead>
        <tbody>
            <c:forEach var="schedule" items="${listS}">
                <tr>
                    <td>${schedule.examName}</td>
                    <td>${schedule.subject}</td>
                    <td>${schedule.roomName}</td>
                    <td>${schedule.slotName}(${schedule.startTime} - ${schedule.endTime})</td>
                    <td>${schedule.date}</td>
                    <td>${not empty schedule.firstName ? schedule.firstName : '<span style="color: red">Not Assigned</span>'}</td>
                    <td>
                        <form action="${pageContext.request.contextPath}/exam-booking" method="post"">
                            <input type="hidden" name="id" value="${schedule.scheduleID}">
                            <!--<input type="hidden" name="action" value="booking">-->
                            <button class="btn btn-danger" type="submit">Booking</button>
                        </form>
                    </td>
                </tr>
            </c:forEach>
        </tbody>
    </table>
    <!-- paging -->
    <nav class="mt-3" aria-label="Page navigation example">
        <ul class="pagination justify-content-center">
            <!-- Disable the "Previous" link if on the first page -->
            <c:if test="${currentPage > 1}">
                <li class="page-item">
                    <a class="page-link" href="?search=${param.search}&amp;page=${currentPage - 1}">Previous</a>
                </li>
            </c:if>

            <!-- Display the page numbers as links -->
            <c:forEach var="pageNum" begin="1" end="${totalPages}">
                <li class="page-item <c:if test='${pageNum == currentPage}'>active</c:if>">
                    <a class="page-link" href="?search=${param.search}&amp;page=${pageNum}">${pageNum}</a>
                </li>
            </c:forEach>

            <!-- Disable the "Next" link if on the last page -->
            <c:if test="${currentPage < totalPages}">
                <li class="page-item">
                    <a class="page-link" href="?search=${param.search}&amp;page=${currentPage + 1}">Next</a>
                </li>
            </c:if>
        </ul>
    </nav>
</main>

<script src="https://code.jquery.com/jquery-3.6.0.min.js"></script>

<!-- Include Bootstrap JS and Popper.js -->
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-YvpcrYf0tY3lHB60NNkmXc5s9fDVZLESaAA55NDzOxhy9GkcIdslK1eN7N6jIeHz" crossorigin="anonymous"></script>
<%@ include file="./view/footer.jsp"%>

