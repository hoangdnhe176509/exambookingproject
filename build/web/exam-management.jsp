<%-- 
    Document   : exam-management
    Created on : Mar 2, 2024, 4:54:28 PM
    Author     : ADMIN
--%>

<%@ page language="java" contentType="text/html; charset=UTF-8"%>
<%@ include file="./view/header.jsp"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<main class="container py-5">
    <div class="row mb-12">
        <div class="col-md-6">
            <div class="form-group">
                <form action="exam-management" method="get">
                    <label for="searchInput" class="form-label">Search:</label>
                    <div class="input-group" style="width: 80%">
                        <input style="width: 80%" type="text" class="form-control" name="search" placeholder="Enter Exam Name or Subject to search!" value="${param.search}">
                        <input type="hidden" name="page" value="${currentPage}">

                    </div>
                </form>
            </div>
        </div>

    </div>
    <h1>Exam Management</h1>
    <c:if test="${not empty param.msg}">
        <div class="alert alert-success" role="alert">
            ${param.msg}
        </div>
    </c:if>

    <!-- Check if there is an error message -->
    <c:if test="${not empty param.err}">
        <div class="alert alert-danger" role="alert">
            ${param.err}
        </div>
    </c:if>
    <button class="btn btn-primary" data-bs-toggle="modal" data-bs-target="#addExamModal">Add Exam</button>

    <table class="table">
        <thead>
            <tr>
                <th>Exam Name</th>
                <th>Subject</th>
                <th>Actions</th>
            </tr>
        </thead>
        <tbody>
            <c:forEach var="exam" items="${listE}">
                <tr>
                    <td>${exam.name}</td>
                    <td>${exam.subject}</td>
                    <td>
                        <div class="d-flex" >
                            <!--<button class="btn btn-primary" data-bs-toggle="modal" data-bs-target="#editExamModal">Add Exam</button>-->
                            <button style="margin-right: 10px" class="btn btn-primary" data-bs-toggle="modal" data-bs-target="#editExamModal${exam.id}">Edit</button>
                            <form action="${pageContext.request.contextPath}/exam-management" method="post" onsubmit="return confirm('Are you sure you want to delete this exam?')">
                                <input type="hidden" name="id" value="${exam.id}">
                                <input type="hidden" name="action" value="delete">
                                <button class="btn btn-danger" type="submit">Delete</button>
                            </form>

                        </div>
                        <div class="modal fade" id="editExamModal${exam.id}" tabindex="-1" role="dialog" aria-labelledby="editExamModal${exam.id}" aria-hidden="true">
                            <div class="modal-dialog" role="document">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <h5 class="modal-title" id="editExamModalLabel-${exam.id}">Edit Exam</h5>
                                    </div>
                                    <div class="modal-body">
                                        <!-- Form for editing the exam, pre-populated with current values -->
                                        <form action="${pageContext.request.contextPath}/exam-management" method="post">
                                            <input type="hidden" name="id" value="${exam.id}">
                                            <input type="hidden" name="action" value="edit">
                                            <div class="form-group">
                                                <label for="editExamName">Exam Name:</label>
                                                <input type="text" class="form-control" id="editExamName" name="name" value="${exam.name}" required>
                                            </div>
                                            <div class="form-group">
                                                <label for="editSubject">Subject:</label>
                                                <input type="text" class="form-control" id="editSubject" name="subject" value="${exam.subject}" required>
                                            </div>
                                            <button type="submit" class="btn btn-primary">Save Changes</button>
                                            <button type="button" class="btn btn-warning" data-bs-dismiss="modal">Cancel</button>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </td>
                </tr>
            </c:forEach>
        </tbody>
    </table>
    <!-- paging -->
    <nav class="mt-3" aria-label="Page navigation example">
        <ul class="pagination justify-content-center">
            <!-- Disable the "Previous" link if on the first page -->
            <c:if test="${currentPage > 1}">
                <li class="page-item">
                    <a class="page-link" href="?category=${param.category}&amp;search=${param.search}&amp;page=${currentPage - 1}">Previous</a>
                </li>
            </c:if>

            <!-- Display the page numbers as links -->
            <c:forEach var="pageNum" begin="1" end="${totalPages}">
                <li class="page-item <c:if test='${pageNum == currentPage}'>active</c:if>">
                    <a class="page-link" href="?category=${param.category}&amp;search=${param.search}&amp;page=${pageNum}">${pageNum}</a>
                </li>
            </c:forEach>

            <!-- Disable the "Next" link if on the last page -->
            <c:if test="${currentPage < totalPages}">
                <li class="page-item">
                    <a class="page-link" href="?category=${param.category}&amp;search=${param.search}&amp;page=${currentPage + 1}">Next</a>
                </li>
            </c:if>
        </ul>
    </nav>

    <!-- Add Exam Modal -->
    <div class="modal fade" id="addExamModal" tabindex="-1" role="dialog" aria-labelledby="addExamModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="addExamModalLabel">Add New Exam</h5>
                </div>
                <div class="modal-body">
                    <!-- Add your form for adding a new exam here -->
                    <form action="${pageContext.request.contextPath}/exam-management" method="post">
                        <!-- Add form fields for Exam Name, Subject, etc. -->
                        <div class="form-group">
                            <label for="examName">Exam Name:</label>
                            <input type="text" class="form-control" id="examName" name="name"  required>
                            <input type="hidden" class="form-control" id="action" name="action" value="add">
                        </div>
                        <div class="form-group">
                            <label for="subject">Subject:</label>
                            <input type="text" class="form-control" id="subject" name="subject" required>
                        </div>
                        <!-- Submit button for adding the exam -->
                        <button type="submit" class="btn btn-primary">Add Exam</button>
                        <!-- Cancel button for closing the modal -->
                        <button type="button" class="btn btn-warning" data-bs-dismiss="modal">Cancel</button>


                    </form>
                </div>
            </div>
        </div>
    </div>

</main>

<script src="https://code.jquery.com/jquery-3.6.0.min.js"></script>

<!-- Include Bootstrap JS and Popper.js -->
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-YvpcrYf0tY3lHB60NNkmXc5s9fDVZLESaAA55NDzOxhy9GkcIdslK1eN7N6jIeHz" crossorigin="anonymous"></script>
<%@ include file="./view/footer.jsp"%>

