<%@ page language="java" contentType="text/html; charset=UTF-8"%>
<%@ include file="./view/header.jsp"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

    <main class="container py-5">
        <h1>Welcome, ${account.firstName} ${account.lastName} !</h1>
    </main>
<%@ include file="./view/footer.jsp"%>
