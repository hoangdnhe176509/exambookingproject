<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page language="java" contentType="text/html; charset=UTF-8"%>
<%@page import="java.time.LocalDate"%>
<%@page import="java.time.DayOfWeek"%>
<%@page import="java.time.format.DateTimeFormatter"%>
<%@page import="java.time.temporal.TemporalAdjusters"%>
<!DOCTYPE html>
<html lang="en">

    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <title>Exam Booking System</title>
        <!-- Bootstrap CSS -->
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.3/css/all.min.css">
        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.3/dist/css/bootstrap.min.css" rel="stylesheet"
              integrity="sha384-QWTKZyjpPEjISv5WaRU9OFeRpok6YctnYmDr5pNlyT2bRjXh0JMhjY6hW+ALEwIH" crossorigin="anonymous">
        <style>
            body {
                min-height: 100vh;
                display: flex;
                flex-direction: column;
            }

            main {
                flex: 1;
            }
        </style>
    </head>

    <body class="bg-light d-flex flex-column">
        <header>
            <!-- Navigation bar for teacher -->
            <nav class="navbar navbar-expand-lg navbar-dark bg-primary">
                <div class="container-fluid">

                    <a class="navbar-brand" href="home.jsp">Exam Booking</a>
                    <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarNav"
                            aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
                        <span class="navbar-toggler-icon"></span>
                    </button>

                    <div class="collapse navbar-collapse justify-content-end" id="navbarNav">
                        <ul class="navbar-nav">
                            <c:if test="${account.role eq 'teacher'}">
                                <li class="nav-item">
                                    <a class="nav-link" href="home.jsp">Home</a>
                                </li>
                                <li class="nav-item">
                                    <%
             LocalDate today = LocalDate.now();
             LocalDate monday = today.with(TemporalAdjusters.previousOrSame(DayOfWeek.MONDAY));
             String startDate = monday.format(DateTimeFormatter.ofPattern("yyyy-MM-dd"));
                                    %>
                                    <a class="nav-link" href="${pageContext.request.contextPath}/teacher-view-schedule?startDate=<%=startDate%>">View Exam Schedule</a>

                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" href="exam-booking">Booking</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" href="teacher-request">Booking Request</a>
                                </li>
                            </c:if>
                            <c:if test="${account.role eq 'admin'}">
                                <li class="nav-item">
                                    <a class="nav-link" href="home.jsp">Home</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" href="exam-management">Exam Management</a>

                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" href="schedule-management">Schedule Management</a>

                                </li>
                                 <li class="nav-item">
                                    <a class="nav-link" href="teacher-request">Request Management</a>

                                </li>
                                <li class="nav-item">
                                    <%
             LocalDate today = LocalDate.now();
             LocalDate monday = today.with(TemporalAdjusters.previousOrSame(DayOfWeek.MONDAY));
             String startDate = monday.format(DateTimeFormatter.ofPattern("yyyy-MM-dd"));
                                    %>
                                    <a class="nav-link" href="${pageContext.request.contextPath}/view-schedule?startDate=<%=startDate%>">View Exam Schedule</a>

                                </li>
                                <!--                                <li class="nav-item">
                                                                    <a class="nav-link" href="quizzes-result">Results</a>
                                                                </li>-->

                            </c:if>
                            <c:if test="${account eq null}">
                                <li class="nav-item">
                                    <a class="nav-link" href="logout">Login</a>
                                </li>
                            </c:if>
                            <c:if test="${account != null}">
                                <li class="nav-item">
                                    <a class="nav-link" href="logout">Logout</a>
                                </li>
                            </c:if>

                        </ul>
                    </div>
                </div>
            </nav>
        </header>
