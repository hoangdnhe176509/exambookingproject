
<%@ page language="java" contentType="text/html; charset=UTF-8"%>
<%@ include file="./view/header.jsp"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<main class="container py-5">
    <div class="row mb-12">
        <div class="col-md-6">
            <div class="form-group">
                <form action="schedule-management" method="get">
                    <label for="searchInput" class="form-label">Search:</label>
                    <div class="input-group" style="width: 80%">
                        <input style="width: 80%" type="text" class="form-control" name="search" placeholder="Enter Exam Name or Subject to search!" value="${param.search}">
                        <input type="hidden" name="page" value="${currentPage}">

                    </div>
                </form>
            </div>
        </div>

    </div>
                        
    <h1>Schedule Management</h1>
     <c:if test="${not empty param.msg}">
        <div class="alert alert-success" role="alert">
            ${param.msg}
        </div>
     </c:if>

    <!-- Check if there is an error message -->
    <c:if test="${not empty param.err}">
        <div class="alert alert-danger" role="alert">
            ${param.err}
        </div>
    </c:if>
    <button class="btn btn-primary" data-bs-toggle="modal" data-bs-target="#addExamScheduleModal">Add Schedule</button>

    <table class="table">
        <thead>
            <tr>
                <th>Exam Type</th>
                <th>Subject</th>
                <th>Room</th>
                <th>Slot</th>
                <th>Date</th>
                <th>Supervisor</th>
                <th>Action</th>
            </tr>
        </thead>
        <tbody>
            <c:forEach var="schedule" items="${listS}">
                <tr>
                    <td>${schedule.examName}</td>
                    <td>${schedule.subject}</td>
                    <td>${schedule.roomName}</td>
                    <td>${schedule.slotName}(${schedule.startTime} - ${schedule.endTime})</td>
                    <td>${schedule.date}</td>
                    <td>${not empty schedule.firstName ? schedule.firstName : '<span style="color: red">Not Assigned</span>'} ${not empty schedule.lastName ? schedule.lastName : ''}</td>
                    <td>
                        <div class="d-flex" >
                            <button style="margin-right: 10px" class="btn btn-primary" data-bs-toggle="modal" data-bs-target="#editExamModal${schedule.scheduleID}">Edit</button>
                            <form action="${pageContext.request.contextPath}/schedule-management" method="post" onsubmit="return confirm('Are you sure you want to delete this schedule?')">
                                <input type="hidden" name="id" value="${schedule.scheduleID}">
                                <input type="hidden" name="action" value="delete">
                                <button class="btn btn-danger" type="submit">Delete</button>
                            </form>

                        </div>
                        <!-- Edit Exam Schedule Modal -->
                        <div class="modal fade" id="editExamModal${schedule.scheduleID}" tabindex="-1" role="dialog" aria-labelledby="editExamModal${schedule.scheduleID}" aria-hidden="true">
                            <div class="modal-dialog" role="document">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <h5 class="modal-title" id="editExamModalLabel-${schedule.scheduleID}">Edit Exam Schedule</h5>
                                    </div>
                                    <div class="modal-body">
                                        <!-- Form for editing the exam schedule, pre-populated with current values -->
                                        <form action="${pageContext.request.contextPath}/schedule-management" method="post">
                                            <input type="hidden" name="id" value="${schedule.scheduleID}">
                                            <input type="hidden" name="action" value="edit">

                                            <!-- Add form fields for editing Exam, Room, Slot, Date, etc. -->
                                            <div class="form-group">
                                                <label for="editSelectExam">Select Exam:</label>
                                                <select class="form-control" id="editSelectExam" name="examId" required>
                                                    <!-- Add options dynamically based on the available exams -->
                                                    <c:forEach var="exam" items="${exams}">
                                                        <option value="${exam.id}" <c:if test="${exam.id eq schedule.examID}">selected</c:if>>${exam.name} - ${exam.subject}</option>
                                                    </c:forEach>
                                                </select>
                                            </div>

                                            <div class="form-group">
                                                <label for="editSelectRoom">Select Room:</label>
                                                <select class="form-control" id="editSelectRoom" name="roomId" required>
                                                    <!-- Add options dynamically based on the available rooms -->
                                                    <c:forEach var="room" items="${rooms}">
                                                        <option value="${room.id}" <c:if test="${room.id eq schedule.roomID}">selected</c:if>>${room.name}</option>
                                                    </c:forEach>
                                                </select>
                                            </div>

                                            <div class="form-group">
                                                <label for="editSelectSlot">Select Slot:</label>
                                                <select class="form-control" id="editSelectSlot" name="slotId" required>
                                                    <!-- Add options dynamically based on the available slots -->
                                                    <c:forEach var="slot" items="${slots}">
                                                        <option value="${slot.id}" <c:if test="${slot.id eq schedule.slotID}">selected</c:if>>${slot.name} - ${slot.start} to ${slot.end}</option>
                                                    </c:forEach>
                                                </select>
                                            </div>

                                            <div class="form-group">
                                                <label for="editExamDate">Exam Date:</label>
                                                <input type="date" class="form-control" id="editExamDate" name="examDate" value="${schedule.date}" required>
                                            </div>

                                            <!-- Add other necessary form fields -->

                                            <!-- Submit button for saving changes -->
                                            <button type="submit" class="btn btn-primary">Save Changes</button>
                                            <!-- Cancel button for closing the modal -->
                                            <button type="button" class="btn btn-warning" data-bs-dismiss="modal">Cancel</button>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </td>
                </tr>
            </c:forEach>
        </tbody>
    </table>
    <!-- paging -->
    <nav class="mt-3" aria-label="Page navigation example">
        <ul class="pagination justify-content-center">
            <!-- Disable the "Previous" link if on the first page -->
            <c:if test="${currentPage > 1}">
                <li class="page-item">
                    <a class="page-link" href="?search=${param.search}&amp;page=${currentPage - 1}">Previous</a>
                </li>
            </c:if>

            <!-- Display the page numbers as links -->
            <c:forEach var="pageNum" begin="1" end="${totalPages}">
                <li class="page-item <c:if test='${pageNum == currentPage}'>active</c:if>">
                    <a class="page-link" href="?search=${param.search}&amp;page=${pageNum}">${pageNum}</a>
                </li>
            </c:forEach>

            <!-- Disable the "Next" link if on the last page -->
            <c:if test="${currentPage < totalPages}">
                <li class="page-item">
                    <a class="page-link" href="?search=${param.search}&amp;page=${currentPage + 1}">Next</a>
                </li>
            </c:if>
        </ul>
    </nav>

    <!-- Add Exam Schedule Modal -->
    <div class="modal fade" id="addExamScheduleModal" tabindex="-1" role="dialog" aria-labelledby="addExamScheduleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="addExamScheduleModalLabel">Add Exam Schedule</h5>
                </div>
                <div class="modal-body">
                    <!-- Your form for adding a new exam schedule goes here -->
                    <form action="${pageContext.request.contextPath}/schedule-management" method="post">
                        <!-- Add form fields for Exam, Room, Slot, Date, etc. -->
                        <div class="form-group">
                            <label for="selectExam">Select Exam:</label>
                            <input type="hidden" name="action" value="add"/>
                            <select class="form-control" id="selectExam" name="examId" required>
                                <option value="">Please select exam</option>
                                <c:forEach var="exam" items="${exams}">
                                    <option value="${exam.id}">${exam.name} - ${exam.subject}</option>
                                </c:forEach>
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="selectRoom">Please select room:</label>
                            <!-- Populate this dropdown with available rooms -->
                            <select class="form-control" id="selectRoom" name="roomId" required>
                                <!-- Add options dynamically based on the available rooms -->
                                <option value="">Select room</option>
                                <c:forEach var="room" items="${rooms}">
                                    <option value="${room.id}">${room.name}</option>
                                </c:forEach>
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="selectSlot">Select Slot:</label>
                            <!-- Populate this dropdown with available slots -->
                            <select class="form-control" id="selectSlot" name="slotId" required>
                                <!-- Add options dynamically based on the available slots -->
                                <c:forEach var="slot" items="${slots}">
                                    <option value="${slot.id}">${slot.name} - ${slot.start} to ${slot.end}</option>
                                </c:forEach>
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="examDate">Exam Date:</label>
                            <input type="date" class="form-control" id="examDate" name="examDate" required>
                        </div>
                        <!-- Add other necessary form fields -->

                        <!-- Submit button for adding the exam schedule -->
                        <button type="submit" class="btn btn-primary">Add Exam Schedule</button>
                        <!-- Cancel button for closing the modal -->
                        <button type="button" class="btn btn-warning" data-bs-dismiss="modal">Cancel</button>
                    </form>
                </div>
            </div>
        </div>
    </div>


</main>

<script src="https://code.jquery.com/jquery-3.6.0.min.js"></script>

<!-- Include Bootstrap JS and Popper.js -->
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-YvpcrYf0tY3lHB60NNkmXc5s9fDVZLESaAA55NDzOxhy9GkcIdslK1eN7N6jIeHz" crossorigin="anonymous"></script>
<%@ include file="./view/footer.jsp"%>

