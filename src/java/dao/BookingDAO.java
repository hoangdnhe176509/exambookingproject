/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package dao;

import java.sql.*;
import java.util.*;
import model.Booking;
import model.ExamSchedule;

/**
 *
 * @author ADMIN
 */
public class BookingDAO extends DBContext {

    public List<Booking> getBookingRequests(int page, int pageSize) {
        List<Booking> bookingRequests = new ArrayList<>();
        List<Object> list = new ArrayList<>();

        try {
            //  SQL để truy xuất yêu cầu đặt chỗ và phân trang
            String query = "SELECT b.RequestID, b.UserID, b.ExamScheduleID, b.RequestDate, b.Status, "
                    + "u.FirstName, u.LastName, e.ExamName, es.Date as ExamDate, r.RoomName, s.SlotName "
                    + "FROM BookingRequests b "
                    + "JOIN Users u ON b.UserID = u.UserID "
                    + "JOIN ExamSchedule es ON b.ExamScheduleID = es.ScheduleID "
                    + "JOIN Exams e ON es.ExamID = e.ExamID "
                    + "JOIN Rooms r ON es.RoomID = r.RoomID "
                    + "JOIN Slots s ON es.SlotID = s.SlotID "
                    + "ORDER BY b.RequestId DESC "
                    + "OFFSET ? ROWS FETCH NEXT ? ROWS ONLY";

            list.add((page - 1) * pageSize);
            list.add(pageSize);

            PreparedStatement preparedStatement = connection.prepareStatement(query);
            mapParams(preparedStatement, list);

            try (ResultSet resultSet = preparedStatement.executeQuery()) {
                while (resultSet.next()) {
                    // Create Booking objects based on the result set
                    Booking booking = new Booking();
                    booking.setId(resultSet.getInt("RequestID"));
                    booking.setUserID(resultSet.getInt("UserID"));
                    booking.setExamScheduleID(resultSet.getInt("ExamScheduleID"));
                    booking.setDate(resultSet.getDate("RequestDate").toLocalDate());
                    booking.setStatus(resultSet.getString("Status"));

                    // Additional details for display
                    booking.setUserName(resultSet.getString("FirstName") + " " + resultSet.getString("LastName"));
                    booking.setExamName(resultSet.getString("ExamName"));
                    booking.setExamDate(resultSet.getDate("ExamDate").toLocalDate());
                    booking.setRoomName(resultSet.getString("RoomName"));
                    booking.setSlotName(resultSet.getString("SlotName"));

                    bookingRequests.add(booking);
                }
            }
        } catch (SQLException e) {
            e.printStackTrace(); // Handle or log the exception as needed
        }

        return bookingRequests;
    }

    // add booking
    public boolean addBookingRequest(Booking booking) {
        try {
            if (isDuplicateBooking(booking.getUserID(), booking.getExamScheduleID())) {
                return false;
            }
            // SQL query to insert a new booking request
            String sql = "INSERT INTO BookingRequests (UserID, ExamScheduleID, RequestDate, Status) VALUES (?, ?, ?, ?)";

            try (PreparedStatement preparedStatement = connection.prepareStatement(sql)) {
                preparedStatement.setInt(1, booking.getUserID());
                preparedStatement.setInt(2, booking.getExamScheduleID());
                preparedStatement.setTimestamp(3, Timestamp.valueOf(booking.getDate().atStartOfDay()));
                preparedStatement.setString(4, booking.getStatus());

                // Execute the insert query and return true if successful
                int rowsAffected = preparedStatement.executeUpdate();
                return rowsAffected > 0;
            }
        } catch (SQLException e) {
            e.printStackTrace(); // Handle or log the exception as needed
            return false; // Return false if an exception occurs
        }
    }

    public List<Booking> getPagedBookingRequestsByUserID(int id, int page, int pageSize) {
        List<Booking> bookingRequests = new ArrayList<>();
        List<Object> list = new ArrayList<>();

        try {
            // SQL query to retrieve booking requests with paging
            String query = "SELECT b.RequestID, b.UserID, b.ExamScheduleID, b.RequestDate, b.Status, "
                    + "u.FirstName, u.LastName, e.ExamName, es.Date as ExamDate, r.RoomName, s.SlotName "
                    + "FROM BookingRequests b "
                    + "JOIN Users u ON b.UserID = u.UserID "
                    + "JOIN ExamSchedule es ON b.ExamScheduleID = es.ScheduleID "
                    + "JOIN Exams e ON es.ExamID = e.ExamID "
                    + "JOIN Rooms r ON es.RoomID = r.RoomID "
                    + "JOIN Slots s ON es.SlotID = s.SlotID "
                    + "WHERE b.userID = ? "
                    + "ORDER BY b.RequestDate DESC "
                    + "OFFSET ? ROWS FETCH NEXT ? ROWS ONLY";
            list.add(id);
            list.add((page - 1) * pageSize); // 7 - 12
            list.add(pageSize);

            PreparedStatement preparedStatement = connection.prepareStatement(query);
            mapParams(preparedStatement, list);

            try (ResultSet resultSet = preparedStatement.executeQuery()) {
                while (resultSet.next()) {
                    // Create Booking objects based on the result set
                    Booking booking = new Booking();
                    booking.setId(resultSet.getInt("RequestID"));
                    booking.setUserID(resultSet.getInt("UserID"));
                    booking.setExamScheduleID(resultSet.getInt("ExamScheduleID"));
                    booking.setDate(resultSet.getDate("RequestDate").toLocalDate());
                    booking.setStatus(resultSet.getString("Status"));

                    // Additional details for display
                    booking.setUserName(resultSet.getString("FirstName") + " " + resultSet.getString("LastName"));
                    booking.setExamName(resultSet.getString("ExamName"));
                    booking.setExamDate(resultSet.getDate("ExamDate").toLocalDate());
                    booking.setRoomName(resultSet.getString("RoomName"));
                    booking.setSlotName(resultSet.getString("SlotName"));

                    bookingRequests.add(booking);
                }
            }
        } catch (SQLException e) {
            e.printStackTrace(); // Handle or log the exception as needed
        }

        return bookingRequests;
    }
   //
    public int getTotalBookingRequestsByUserID(int userID) {
        int totalBookingRequests = 0;

        try {
            // SQL query to retrieve the total number of booking requests for a user
            String sql = "SELECT COUNT(*) AS total FROM BookingRequests WHERE UserID = ?";

            try (PreparedStatement preparedStatement = connection.prepareStatement(sql)) {
                preparedStatement.setInt(1, userID);

                try (ResultSet resultSet = preparedStatement.executeQuery()) {
                    if (resultSet.next()) {
                        totalBookingRequests = resultSet.getInt("total");
                    }
                }
            }
        } catch (SQLException e) {
            e.printStackTrace(); // Handle or log the exception as needed
        }

        return totalBookingRequests;
    }

    public int getTotalBookingRequests() {
        int totalBookingRequests = 0;

        try {
            // SQL query to retrieve the total number of booking requests for a user
            String sql = "SELECT COUNT(*) AS total FROM BookingRequests";

            try (PreparedStatement preparedStatement = connection.prepareStatement(sql)) {

                try (ResultSet resultSet = preparedStatement.executeQuery()) {
                    if (resultSet.next()) {
                        totalBookingRequests = resultSet.getInt("total");
                    }
                }
            }
        } catch (SQLException e) {
            e.printStackTrace(); // Handle or log the exception as needed
        }

        return totalBookingRequests;
    }

    public boolean cancelBooking(int bookingId) {
        try {
            // Logic to update the booking status to "Cancel" in the database
            String query = "UPDATE BookingRequests SET Status = 'Cancel' WHERE RequestID = ?";
            PreparedStatement preparedStatement = connection.prepareStatement(query);
            preparedStatement.setInt(1, bookingId);

            // Execute the update statement
            int rowsAffected = preparedStatement.executeUpdate();

            // Check if the update was successful
            return rowsAffected > 0;
        } catch (SQLException e) {
            e.printStackTrace();
            // Handle or log the exception as needed
            return false;
        }

    }
    // check
    public boolean isDuplicateBooking(int userID, int examScheduleID) {
        try {
            // Check if there is an existing booking with the same UserID and ExamScheduleID
            String query = "SELECT COUNT(*) AS count FROM BookingRequests WHERE UserID = ? AND ExamScheduleID = ?";
            try (PreparedStatement preparedStatement = connection.prepareStatement(query)) {
                preparedStatement.setInt(1, userID);
                preparedStatement.setInt(2, examScheduleID);

                try (ResultSet resultSet = preparedStatement.executeQuery()) {
                    if (resultSet.next()) {
                        int count = resultSet.getInt("count");
                        return count > 0;
                    }
                }
            }
        } catch (SQLException e) {
            e.printStackTrace(); // Handle or log the exception as needed
        }

        // Return false in case of an exception or if no duplicate is found
        return false;
    }

    public Booking getBookingById(int bookingId) {
        Booking booking = null;
        try {
            String query = "SELECT * FROM BookingRequests WHERE RequestID = ?";
            try (PreparedStatement preparedStatement = connection.prepareStatement(query)) {
                preparedStatement.setInt(1, bookingId);
                try (ResultSet resultSet = preparedStatement.executeQuery()) {
                    if (resultSet.next()) {
                        booking = new Booking();
                        booking.setId(resultSet.getInt("RequestID"));
                        booking.setUserID(resultSet.getInt("UserID"));
                        booking.setExamScheduleID(resultSet.getInt("ExamScheduleID"));
                        booking.setDate(resultSet.getDate("RequestDate").toLocalDate());
                        booking.setStatus(resultSet.getString("Status"));
                    }
                }
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return booking;
    }

    public boolean approveBooking(int bookingId) {
        try {
            String query = "UPDATE BookingRequests SET Status = 'Approve' WHERE RequestID = ?";
            try (PreparedStatement preparedStatement = connection.prepareStatement(query)) {
                preparedStatement.setInt(1, bookingId);
                int rowsAffected = preparedStatement.executeUpdate();
                return rowsAffected > 0;
            }
        } catch (SQLException e) {
            e.printStackTrace();
            return false;
        }
    }
    // cancel nếu 
    public boolean cancelOtherBookings(int examScheduleID) {
        try {
            String query = "UPDATE BookingRequests SET Status = 'Cancel' WHERE ExamScheduleID = ? AND Status = 'Pending'";
            try (PreparedStatement preparedStatement = connection.prepareStatement(query)) {
                preparedStatement.setInt(1, examScheduleID);
                int rowsAffected = preparedStatement.executeUpdate();
                return rowsAffected > 0;
            }
        } catch (SQLException e) {
            e.printStackTrace();
            return false;
        }
    }

    public int calculateTotalPages(int userID, int pageSize) {
        int totalBookingRequests = getTotalBookingRequestsByUserID(userID);
        return (int) Math.ceil((double) totalBookingRequests / pageSize);
    }

    public int calculateTotalPages(int pageSize) {
        int totalBookingRequests = getTotalBookingRequests();
        return (int) Math.ceil((double) totalBookingRequests / pageSize);
    }

    public ExamSchedule getExamScheduleByBookingID(int requestID) {
        ExamSchedule examSchedule = null;

        try {
            String query = "SELECT es.ExamID, es.RoomID, es.SlotID, es.date "
                    + "FROM ExamSchedule es "
                    + "JOIN BookingRequests br ON es.ScheduleID = br.ExamScheduleID "
                    + "WHERE br.RequestID = ?";
            try (PreparedStatement preparedStatement = connection.prepareStatement(query)) {
                preparedStatement.setInt(1, requestID);

                try (ResultSet resultSet = preparedStatement.executeQuery()) {
                    if (resultSet.next()) {
                        examSchedule = new ExamSchedule();
                        examSchedule.setExamID(resultSet.getInt("ExamID"));
                        examSchedule.setRoomID(resultSet.getInt("RoomID"));
                        examSchedule.setSlotID(resultSet.getInt("SlotID"));
                        examSchedule.setDate(resultSet.getDate("date").toLocalDate());
                    }
                }
            }
        } catch (SQLException e) {
            e.printStackTrace(); // Handle or log the exception as needed
        }

        return examSchedule;
    }
    public void mapParams(PreparedStatement ps, List<Object> args) throws SQLException {
        int i = 1;
        for (Object arg : args) {
            if (arg instanceof java.sql.Date) {
                ps.setTimestamp(i++, new Timestamp(((java.sql.Date) arg).getTime()));
            } else if (arg instanceof Integer) {
                ps.setInt(i++, (Integer) arg);
            } else if (arg instanceof Long) {
                ps.setLong(i++, (Long) arg);
            } else if (arg instanceof Double) {
                ps.setDouble(i++, (Double) arg);
            } else if (arg instanceof Float) {
                ps.setFloat(i++, (Float) arg);
            } else {
                ps.setString(i++, (String) arg);
            }

        }
    }

    public static void main(String[] args) {
        BookingDAO bookingDAO = new BookingDAO();
        System.out.println(bookingDAO.calculateTotalPages(3));
    }
    
    
}
