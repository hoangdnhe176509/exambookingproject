/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package dao;

import dao.DBContext;
import dao.ExamDAO;
import java.sql.*;
import java.time.LocalDate;
import java.util.*;
import model.ExamSchedule;

/**
 *
 * @author ADMIN
 */
public class ScheduleDAO extends DBContext {

    public List<ExamSchedule> getExamSchedulesByUserID(int userID, LocalDate startDate, LocalDate endDate) {
        List<ExamSchedule> examSchedules = new ArrayList<>();

        // SQL query to retrieve exam schedules for a user within a date range
        String sql = "SELECT es.ScheduleID, es.UserID, es.ExamID, es.RoomID, es.SlotID, es.Date, "
                + "u.FirstName, u.LastName, e.ExamName, e.Subject, r.RoomName, s.SlotName, s.StartTime, s.EndTime "
                + "FROM ExamSchedule es "
                + "JOIN Users u ON es.UserID = u.UserID "
                + "JOIN Exams e ON es.ExamID = e.ExamID "
                + "JOIN Rooms r ON es.RoomID = r.RoomID "
                + "JOIN Slots s ON es.SlotID = s.SlotID "
                + "WHERE es.UserID = ? AND es.Date BETWEEN ? AND ?";

        try {
            PreparedStatement preparedStatement = connection.prepareStatement(sql);
            preparedStatement.setInt(1, userID);
            preparedStatement.setDate(2, java.sql.Date.valueOf(startDate));
            preparedStatement.setDate(3, java.sql.Date.valueOf(endDate));

            try ( ResultSet resultSet = preparedStatement.executeQuery()) {
                while (resultSet.next()) {
                    // Create ExamSchedule objects based on the result set
                    ExamSchedule examSchedule = new ExamSchedule();
                    examSchedule.setScheduleID(resultSet.getInt("ScheduleID"));
                    examSchedule.setUserID(resultSet.getInt("UserID"));
                    examSchedule.setExamID(resultSet.getInt("ExamID"));
                    examSchedule.setRoomID(resultSet.getInt("RoomID"));
                    examSchedule.setSlotID(resultSet.getInt("SlotID"));
                    examSchedule.setDate(resultSet.getDate("Date").toLocalDate());

                    // Additional details for display
                    examSchedule.setFirstName(resultSet.getString("FirstName"));
                    examSchedule.setLastName(resultSet.getString("LastName"));
                    examSchedule.setExamName(resultSet.getString("ExamName"));
                    examSchedule.setSubject(resultSet.getString("Subject"));
                    examSchedule.setRoomName(resultSet.getString("RoomName"));
                    examSchedule.setSlotName(resultSet.getString("SlotName"));
                    examSchedule.setStartTime(resultSet.getTime("StartTime").toLocalTime());
                    examSchedule.setEndTime(resultSet.getTime("EndTime").toLocalTime());

                    // Add the ExamSchedule object to the list
                    examSchedules.add(examSchedule);
                }
            }
        } catch (SQLException e) {
            e.printStackTrace(); // Handle or log the exception as needed
        }

        return examSchedules;
    }

    public List<ExamSchedule> getExamSchedules(LocalDate startDate, LocalDate endDate) {
        List<ExamSchedule> examSchedules = new ArrayList<>();

        // SQL query to retrieve exam schedules for a user within a date range
        String sql = "SELECT es.ScheduleID, es.UserID, es.ExamID, es.RoomID, es.SlotID, es.Date, "
                + "u.FirstName, u.LastName, e.ExamName, e.Subject, r.RoomName, s.SlotName, s.StartTime, s.EndTime "
                + "FROM ExamSchedule es "
                + "LEFT JOIN Users u ON es.UserID = u.UserID "
                + "JOIN Exams e ON es.ExamID = e.ExamID "
                + "JOIN Rooms r ON es.RoomID = r.RoomID "
                + "JOIN Slots s ON es.SlotID = s.SlotID "
                + "WHERE es.Date BETWEEN ? AND ?";

        try {
            PreparedStatement preparedStatement = connection.prepareStatement(sql);
            preparedStatement.setDate(1, java.sql.Date.valueOf(startDate));
            preparedStatement.setDate(2, java.sql.Date.valueOf(endDate));

            try ( ResultSet resultSet = preparedStatement.executeQuery()) {
                while (resultSet.next()) {
                    // Create ExamSchedule objects based on the result set
                    ExamSchedule examSchedule = new ExamSchedule();
                    examSchedule.setScheduleID(resultSet.getInt("ScheduleID"));
                    examSchedule.setUserID(resultSet.getInt("UserID"));
                    examSchedule.setExamID(resultSet.getInt("ExamID"));
                    examSchedule.setRoomID(resultSet.getInt("RoomID"));
                    examSchedule.setSlotID(resultSet.getInt("SlotID"));
                    examSchedule.setDate(resultSet.getDate("Date").toLocalDate());

                    // Additional details for display
                    examSchedule.setFirstName(resultSet.getString("FirstName"));
                    examSchedule.setLastName(resultSet.getString("LastName"));
                    examSchedule.setExamName(resultSet.getString("ExamName"));
                    examSchedule.setSubject(resultSet.getString("Subject"));
                    examSchedule.setRoomName(resultSet.getString("RoomName"));
                    examSchedule.setSlotName(resultSet.getString("SlotName"));
                    examSchedule.setStartTime(resultSet.getTime("StartTime").toLocalTime());
                    examSchedule.setEndTime(resultSet.getTime("EndTime").toLocalTime());

                    // Add the ExamSchedule object to the list
                    examSchedules.add(examSchedule);
                }
            }
        } catch (SQLException e) {
            e.printStackTrace(); // Handle or log the exception as needed
        }

        return examSchedules;
    }

    public List<ExamSchedule> getAllExamSchedulesNotAssigned(String searchParam) {
        List<ExamSchedule> examSchedules = new ArrayList<>();
        List<Object> list = new ArrayList<>();

        try {
            StringBuilder query = new StringBuilder();
            // SQL query to retrieve exam schedules for a user within a date range
            query.append("SELECT es.ScheduleID, es.UserID, es.ExamID, es.RoomID, es.SlotID, es.Date, "
                    + "u.FirstName, u.LastName, e.ExamName, e.Subject, r.RoomName, s.SlotName, s.StartTime, s.EndTime "
                    + "FROM ExamSchedule es "
                    + "LEFT JOIN Users u ON es.UserID = u.UserID "
                    + "JOIN Exams e ON es.ExamID = e.ExamID "
                    + "JOIN Rooms r ON es.RoomID = r.RoomID "
                    + "JOIN Slots s ON es.SlotID = s.SlotID "
                    + "WHERE es.UserID IS NULL");
            if (searchParam != null && !searchParam.trim().isEmpty()) {
                query.append(" AND ((CONCAT(u.FirstName, ' ', u.LastName) LIKE ?) OR Subject LIKE ?)");
                list.add("%" + searchParam + "%");
                list.add("%" + searchParam + "%");
            }
            query.append(" ORDER BY es.Date DESC");
            PreparedStatement preparedStatement = connection.prepareStatement(query.toString());
            mapParams(preparedStatement, list);
            try ( ResultSet resultSet = preparedStatement.executeQuery()) {
                while (resultSet.next()) {
                    // Create ExamSchedule objects based on the result set
                    ExamSchedule examSchedule = new ExamSchedule();
                    examSchedule.setScheduleID(resultSet.getInt("ScheduleID"));
                    examSchedule.setUserID(resultSet.getInt("UserID"));
                    examSchedule.setExamID(resultSet.getInt("ExamID"));
                    examSchedule.setRoomID(resultSet.getInt("RoomID"));
                    examSchedule.setSlotID(resultSet.getInt("SlotID"));
                    examSchedule.setDate(resultSet.getDate("Date").toLocalDate());

                    // Additional details for display
                    examSchedule.setFirstName(resultSet.getString("FirstName"));
                    examSchedule.setLastName(resultSet.getString("LastName"));
                    examSchedule.setExamName(resultSet.getString("ExamName"));
                    examSchedule.setSubject(resultSet.getString("Subject"));
                    examSchedule.setRoomName(resultSet.getString("RoomName"));
                    examSchedule.setSlotName(resultSet.getString("SlotName"));
                    examSchedule.setStartTime(resultSet.getTime("StartTime").toLocalTime());
                    examSchedule.setEndTime(resultSet.getTime("EndTime").toLocalTime());

                    // Add the ExamSchedule object to the list
                    examSchedules.add(examSchedule);
                }
            }
        } catch (SQLException e) {
            e.printStackTrace(); // Handle or log the exception as needed
        }

        return examSchedules;
    }

    public List<ExamSchedule> getAllExamSchedules(String searchParam) {
        List<ExamSchedule> examSchedules = new ArrayList<>();
        List<Object> list = new ArrayList<>();

        try {
            StringBuilder query = new StringBuilder();
            // SQL query to retrieve exam schedules for a user within a date range
            query.append("SELECT es.ScheduleID, es.UserID, es.ExamID, es.RoomID, es.SlotID, es.Date, "
                    + "u.FirstName, u.LastName, e.ExamName, e.Subject, r.RoomName, s.SlotName, s.StartTime, s.EndTime "
                    + "FROM ExamSchedule es "
                    + "LEFT JOIN Users u ON es.UserID = u.UserID "
                    + "JOIN Exams e ON es.ExamID = e.ExamID "
                    + "JOIN Rooms r ON es.RoomID = r.RoomID "
                    + "JOIN Slots s ON es.SlotID = s.SlotID "
                    + "WHERE 1 = 1");
            if (searchParam != null && !searchParam.trim().isEmpty()) {
                query.append(" AND ((CONCAT(u.FirstName, ' ', u.LastName) LIKE ?) OR Subject LIKE ?)");
                list.add("%" + searchParam + "%");
                list.add("%" + searchParam + "%");
            }
            query.append(" ORDER BY es.Date DESC");
            PreparedStatement preparedStatement = connection.prepareStatement(query.toString());
            mapParams(preparedStatement, list);
            try ( ResultSet resultSet = preparedStatement.executeQuery()) {
                while (resultSet.next()) {
                    // Create ExamSchedule objects based on the result set
                    ExamSchedule examSchedule = new ExamSchedule();
                    examSchedule.setScheduleID(resultSet.getInt("ScheduleID"));
                    examSchedule.setUserID(resultSet.getInt("UserID"));
                    examSchedule.setExamID(resultSet.getInt("ExamID"));
                    examSchedule.setRoomID(resultSet.getInt("RoomID"));
                    examSchedule.setSlotID(resultSet.getInt("SlotID"));
                    examSchedule.setDate(resultSet.getDate("Date").toLocalDate());

                    // Additional details for display
                    examSchedule.setFirstName(resultSet.getString("FirstName"));
                    examSchedule.setLastName(resultSet.getString("LastName"));
                    examSchedule.setExamName(resultSet.getString("ExamName"));
                    examSchedule.setSubject(resultSet.getString("Subject"));
                    examSchedule.setRoomName(resultSet.getString("RoomName"));
                    examSchedule.setSlotName(resultSet.getString("SlotName"));
                    examSchedule.setStartTime(resultSet.getTime("StartTime").toLocalTime());
                    examSchedule.setEndTime(resultSet.getTime("EndTime").toLocalTime());

                    // Add the ExamSchedule object to the list
                    examSchedules.add(examSchedule);
                }
            }
        } catch (SQLException e) {
            e.printStackTrace(); // Handle or log the exception as needed
        }

        return examSchedules;
    }

    public void mapParams(PreparedStatement ps, List<Object> args) throws SQLException {
        int i = 1;
        for (Object arg : args) {
            if (arg instanceof java.sql.Date) {
                ps.setTimestamp(i++, new Timestamp(((java.sql.Date) arg).getTime()));
            } else if (arg instanceof Integer) {
                ps.setInt(i++, (Integer) arg);
            } else if (arg instanceof Long) {
                ps.setLong(i++, (Long) arg);
            } else if (arg instanceof Double) {
                ps.setDouble(i++, (Double) arg);
            } else if (arg instanceof Float) {
                ps.setFloat(i++, (Float) arg);
            } else {
                ps.setString(i++, (String) arg);
            }

        }
    }

    public List<ExamSchedule> Paging(List<ExamSchedule> exams, int page, int pageSize) {
        int fromIndex = (page - 1) * pageSize;
        int toIndex = Math.min(fromIndex + pageSize, exams.size());

        if (fromIndex > toIndex) {
            // Handle the case where fromIndex is greater than toIndex
            fromIndex = toIndex;
        }

        return exams.subList(fromIndex, toIndex);
    }

    public boolean isDuplicateSchedule(int roomId, int slotId, LocalDate date) {
        boolean isDuplicate = false;

        try {
            // SQL query to check for a duplicate schedule
            String sql = "SELECT COUNT(*) AS count FROM ExamSchedule "
                    + "WHERE RoomID = ? AND SlotID = ? AND Date = ?";

            try ( PreparedStatement preparedStatement = connection.prepareStatement(sql)) {
                preparedStatement.setInt(1, roomId);
                preparedStatement.setInt(2, slotId);
                preparedStatement.setDate(3, java.sql.Date.valueOf(date));

                try ( ResultSet resultSet = preparedStatement.executeQuery()) {
                    if (resultSet.next()) {
                        int count = resultSet.getInt("count");
                        isDuplicate = count > 0;
                    }
                }
            }
        } catch (SQLException e) {
            e.printStackTrace(); // Handle or log the exception as needed
        }

        return isDuplicate;
    }

    public boolean addExamSchedule(int examId, int roomId, int slotId, java.sql.Date date) {
        try {
            // SQL query to insert a new exam schedule without specifying UserID
            String sql = "INSERT INTO ExamSchedule (ExamID, RoomID, SlotID, Date) VALUES (?, ?, ?, ?)";

            try ( PreparedStatement preparedStatement = connection.prepareStatement(sql)) {
                preparedStatement.setInt(1, examId);
                preparedStatement.setInt(2, roomId);
                preparedStatement.setInt(3, slotId);
                preparedStatement.setDate(4, date);

                // Execute the insert query and return true if successful
                int rowsAffected = preparedStatement.executeUpdate();
                return rowsAffected > 0;
            }
        } catch (SQLException e) {
            e.printStackTrace(); // Handle or log the exception as needed
            return false; // Return false if an exception occurs
        }
    }

    public boolean updateExamScheduleUserID(int userID, int examId, int roomId, int slotId, LocalDate date) {
        try {
            // SQL query to update the UserID in ExamSchedule based on specified conditions
            String sql = "UPDATE ExamSchedule SET UserID = ? WHERE ExamID = ? AND RoomID = ? AND SlotID = ? AND Date = ?";

            try ( PreparedStatement preparedStatement = connection.prepareStatement(sql)) {
                preparedStatement.setInt(1, userID);
                preparedStatement.setInt(2, examId);
                preparedStatement.setInt(3, roomId);
                preparedStatement.setInt(4, slotId);
                preparedStatement.setDate(5, java.sql.Date.valueOf(date));

                // Execute the update query and return true if successful
                int rowsAffected = preparedStatement.executeUpdate();
                return rowsAffected > 0;
            }
        } catch (SQLException e) {
            e.printStackTrace(); // Handle or log the exception as needed
            return false; // Return false if an exception occurs
        }
    }

    public boolean hasDuplicateUserInRoom(int userID, int slotId, LocalDate date) {
        try {
            String sql = "SELECT COUNT(*) FROM ExamSchedule WHERE UserID = ? AND SlotID = ? and date = ?";
            try ( PreparedStatement preparedStatement = connection.prepareStatement(sql)) {
                preparedStatement.setInt(1, userID);
                preparedStatement.setInt(2, slotId);
                preparedStatement.setDate(3, java.sql.Date.valueOf(date));
                ResultSet resultSet = preparedStatement.executeQuery();
                if (resultSet.next() && resultSet.getInt(1) > 0) {
                    return true; // Duplicate record found
                }
            }
        } catch (SQLException e) {
            e.printStackTrace(); // Handle or log the exception as needed
        }
        return false; // No duplicate record found or exception occurred
    }

    public boolean updateExamSchedule(ExamSchedule updatedSchedule) {
        try {
            // SQL query to update an existing exam schedule
            String sql = "UPDATE ExamSchedule SET ExamID = ?, RoomID = ?, SlotID = ?, Date = ? WHERE ScheduleID = ?";

            try ( PreparedStatement preparedStatement = connection.prepareStatement(sql)) {
                preparedStatement.setInt(1, updatedSchedule.getExamID());
                preparedStatement.setInt(2, updatedSchedule.getRoomID());
                preparedStatement.setInt(3, updatedSchedule.getSlotID());
                preparedStatement.setDate(4, java.sql.Date.valueOf(updatedSchedule.getDate()));
                preparedStatement.setInt(5, updatedSchedule.getScheduleID());

                // Execute the update query and return true if successful
                int rowsAffected = preparedStatement.executeUpdate();
                return rowsAffected > 0;
            }
        } catch (SQLException e) {
            e.printStackTrace(); // Handle or log the exception as needed
            return false; // Return false if an exception occurs
        }
    }

    public boolean deleteExamSchedule(int scheduleId) {
        try {
            // SQL query to delete an exam schedule by ScheduleID
            String sql = "DELETE FROM ExamSchedule WHERE ScheduleID = ?";

            try ( PreparedStatement preparedStatement = connection.prepareStatement(sql)) {
                preparedStatement.setInt(1, scheduleId);

                // Execute the delete query and return true if successful
                int rowsAffected = preparedStatement.executeUpdate();
                return rowsAffected > 0;
            }
        } catch (SQLException e) {
            e.printStackTrace(); // Handle or log the exception as needed
            return false; // Return false if an exception occurs
        }
    }

   public static void main(String[] args) {
        ScheduleDAO examScheduleDAO = new ScheduleDAO();
        List<ExamSchedule> examSchedules = examScheduleDAO.getAllExamSchedulesNotAssigned("");
        LocalDate date = LocalDate.of(2024, 8, 17); // Create a LocalDate object with the specified date
        boolean hasDuplicate = examScheduleDAO.hasDuplicateUserInRoom(2, 1, date);
        System.out.println("Duplicate user in room: " + hasDuplicate);
    }
}
