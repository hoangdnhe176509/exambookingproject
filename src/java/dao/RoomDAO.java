/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package dao;

import java.util.*;
import java.sql.*;
import model.Room;

/**
 *
 * @author ADMIN
 */
public class RoomDAO extends DBContext {

    public List<Room> getAllRooms() {
        List<Room> exams = new ArrayList<>();

        PreparedStatement stm = null;
        ResultSet rs = null;

        try {
            String sql = "SELECT * FROM Rooms";
            stm = connection.prepareStatement(sql);
            rs = stm.executeQuery();

            while (rs.next()) {
                int id = rs.getInt("RoomID");
                String name = rs.getString("RoomName");
                Room room = new Room(id, name);
                exams.add(room);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return exams;
    }
}
