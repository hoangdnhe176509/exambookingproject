/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package dao;

import java.util.*;
import java.sql.*;
import java.time.LocalTime;
import model.*;

/**
 *
 * @author ADMIN
 */
public class SlotDAO extends DBContext {

    public List<Slot> getAllSlots() {
        List<Slot> slots = new ArrayList<>();

        PreparedStatement stm = null;
        ResultSet rs = null;

        try {
            String sql = "SELECT * FROM Slots";
            stm = connection.prepareStatement(sql);
            rs = stm.executeQuery();

            while (rs.next()) {
                int id = rs.getInt("SlotID");
                String name = rs.getString("SlotName");
                LocalTime start = rs.getTime("StartTime").toLocalTime();
                LocalTime end = rs.getTime("EndTime").toLocalTime();
                Slot slot = new Slot(id, name, start, end);
                slots.add(slot);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return slots;
    }

    public static void main(String[] args) {
        SlotDAO o = new SlotDAO();
        List<Slot> l = o.getAllSlots();
        for (Slot r : l) {
            System.out.println(r);
        }
    }
}
