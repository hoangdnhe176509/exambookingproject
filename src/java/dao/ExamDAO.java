/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package dao;

import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import model.Exam;
import model.ExamSchedule;

/**
 *
 * @author ADMIN
 */
public class ExamDAO extends DBContext {

    // search exam by exam name or subject name
    public List<Exam> getAllExamWithParam(String searchParam) {
        List<Exam> exams = new ArrayList<>();
        List<Object> list = new ArrayList<>();
        PreparedStatement stm = null;
        ResultSet rs = null;

        try {
            StringBuilder query = new StringBuilder();
            // khời tạo query 
            query.append("SELECT * FROM Exams where 1=1");
            // nếu tham số sreach khác null thì nối chuỗi
            if (searchParam != null && !searchParam.trim().isEmpty()) {
                query.append(" AND (ExamName LIKE ? OR Subject LIKE ?)");
                list.add("%" + searchParam + "%");
                list.add("%" + searchParam + "%");
            }
            query.append(" ORDER BY ExamID DESC");
            stm = connection.prepareStatement(query.toString());
            mapParams(stm, list);
            rs = stm.executeQuery();

            while (rs.next()) {
                int id = rs.getInt("ExamID");
                String name = rs.getString("ExamName");
                String subject = rs.getString("Subject");
                Exam exam = new Exam(id, name, subject);
                exams.add(exam);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return exams;
    }

    public List<Exam> getAllExams() {
        List<Exam> exams = new ArrayList<>();

        PreparedStatement stm = null;
        ResultSet rs = null;

        try {
            String sql = "SELECT * FROM Exams";
            stm = connection.prepareStatement(sql);
            rs = stm.executeQuery();
            while (rs.next()) {
                int id = rs.getInt("ExamID");
                String name = rs.getString("ExamName");
                String subject = rs.getString("Subject");
                Exam exam = new Exam(id, name, subject);
                exams.add(exam);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return exams;
    }

    // phan trang cho list
    public List<Exam> Paging(List<Exam> exams, int page, int pageSize) {
        int fromIndex = (page - 1) * pageSize;
        int toIndex = Math.min(fromIndex + pageSize, exams.size());

        if (fromIndex > toIndex) {
            // Handle the case where fromIndex is greater than toIndex
            fromIndex = toIndex;
        }

        return exams.subList(fromIndex, toIndex);
    }

    public boolean addExam(Exam exam) {
        String query = "INSERT INTO Exams (ExamName, Subject) VALUES (?, ?)";
        try (PreparedStatement stm = connection.prepareStatement(query)) {
            stm.setString(1, exam.getName());
            stm.setString(2, exam.getSubject());

            int rowsAffected = stm.executeUpdate();
            return rowsAffected > 0;
        } catch (SQLException e) {
            e.printStackTrace();
            return false;
        }
    }

    // edit exam by id 
    public boolean editExam(Exam exam) {
        String query = "UPDATE Exams SET ExamName = ?, Subject = ? WHERE ExamID = ?";
        try (PreparedStatement stm = connection.prepareStatement(query)) {
            stm.setString(1, exam.getName());
            stm.setString(2, exam.getSubject());
            stm.setInt(3, exam.getId());

            int rowsAffected = stm.executeUpdate();
            return rowsAffected > 0;
        } catch (SQLException e) {
            e.printStackTrace();
            return false;
        }
    }

    // delete exam by id
    public boolean deleteExam(int examID) {
        String query = "DELETE FROM Exams WHERE ExamID = ?";
        try (PreparedStatement stm = connection.prepareStatement(query)) {
            stm.setInt(1, examID);

            int rowsAffected = stm.executeUpdate();
            return rowsAffected > 0;
        } catch (SQLException e) {
            e.printStackTrace();
            return false;
        }
    }

    public boolean examExists(String name) {
        boolean isDuplicate = false;

        try {
            // SQL query to check for a duplicate schedule
            String sql = "SELECT COUNT(*) AS count FROM Exams \n"
                    + "  WHERE Exams.ExamName like ?";

            try (PreparedStatement preparedStatement = connection.prepareStatement(sql)) {
                preparedStatement.setString(1, name);
                try (ResultSet resultSet = preparedStatement.executeQuery()) {
                    if (resultSet.next()) {
                        int count = resultSet.getInt("count");
                        isDuplicate = count > 0;
                    }
                }
            }
        } catch (SQLException e) {
            e.printStackTrace(); // Handle or log the exception as needed
        }

        return isDuplicate;
    }

    public void mapParams(PreparedStatement ps, List<Object> args) throws SQLException {
        int i = 1;
        for (Object arg : args) {
            if (arg instanceof Date) {
                ps.setTimestamp(i++, new Timestamp(((Date) arg).getTime()));
            } else if (arg instanceof Integer) {
                ps.setInt(i++, (Integer) arg);
            } else if (arg instanceof Long) {
                ps.setLong(i++, (Long) arg);
            } else if (arg instanceof Double) {
                ps.setDouble(i++, (Double) arg);
            } else if (arg instanceof Float) {
                ps.setFloat(i++, (Float) arg);
            } else {
                ps.setString(i++, (String) arg);
            }

        }
    }

    public static void main(String[] args) {
        ExamDAO e = new ExamDAO();
        System.out.println(e.examExists("PRC392c"));
    }
}
