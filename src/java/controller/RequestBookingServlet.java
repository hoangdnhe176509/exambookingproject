/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */
package controller;

import dao.BookingDAO;
import dao.ScheduleDAO;
import jakarta.servlet.RequestDispatcher;
import java.io.IOException;
import java.io.PrintWriter;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;
import java.util.List;
import model.Booking;
import model.ExamSchedule;
import model.User;

/**
 *
 * @author ADMIN
 */
@WebServlet(name = "TeacherRequestBookingServlet", urlPatterns = {"/teacher-request"})
public class RequestBookingServlet extends HttpServlet {

    @Override
// Inside your servlet code
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        HttpSession session = request.getSession();
        User account = (User) session.getAttribute("account");
        BookingDAO bookingDAO = new BookingDAO();
        if (account != null) {
            if (account.getRole().equals("teacher")) {
                String pageParam = request.getParameter("page");
                int page = 1; // Default to the first page
                int pageSize = 6; // Set the desired page size
                if (pageParam != null && !pageParam.isEmpty()) {
                    // Parse the page parameter to an integer
                    page = Integer.parseInt(pageParam);
                }

                // Retrieve a subset of booking requests based on paging parameters
                List<Booking> pagedBookingRequests = bookingDAO.getPagedBookingRequestsByUserID(account.getId(), page, pageSize);

                // Set the pagedBookingRequests and other paging information as request attributes
                request.setAttribute("pagedBookingRequests", pagedBookingRequests);
                request.setAttribute("currentPage", page);
                request.setAttribute("totalPages", bookingDAO.calculateTotalPages(account.getId(), pageSize));

                // Forward to the JSP page
                request.getRequestDispatcher("/booing-request.jsp").forward(request, response);
                
            } else if (account.getRole().equals("admin")) {
                String pageParam = request.getParameter("page");
                int page = 1; // Default to the first page
                int pageSize = 6; // Set the desired page size
                if (pageParam != null && !pageParam.isEmpty()) {
                    // Parse the page parameter to an integer
                    page = Integer.parseInt(pageParam);
                }
                List<Booking> pagedBookingRequests = bookingDAO.getBookingRequests(page, pageSize);

                // Set the pagedBookingRequests and other paging information as request attributes
                request.setAttribute("pagedBookingRequests", pagedBookingRequests);
                request.setAttribute("currentPage", page);
                request.setAttribute("totalPages", bookingDAO.calculateTotalPages (pageSize));
                RequestDispatcher dispatcher = request.getRequestDispatcher("/booing-request.jsp");
                dispatcher.forward(request, response);
            }

        } else {
            response.sendRedirect("login?err=You must login first!");
        }
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        HttpSession session = request.getSession();
        User account = (User) session.getAttribute("account");
        BookingDAO bookingDAO = new BookingDAO();
        ScheduleDAO scheduleDAO = new ScheduleDAO();
        if (account != null) {
            String action = request.getParameter("action");
            if (action.equals("cancel")) {
                int bookingId = Integer.parseInt(request.getParameter("bookingId"));
                boolean isCancel = bookingDAO.cancelBooking(bookingId);
                if (isCancel) {
                    response.sendRedirect("teacher-request?msg=Cancel successfully!!");
                }
            } else if (action.equals("approve")) {
                int bookingId = Integer.parseInt(request.getParameter("bookingId"));

                // Get the booking details
                Booking booking = bookingDAO.getBookingById(bookingId);
                ExamSchedule examSchedule = bookingDAO.getExamScheduleByBookingID(bookingId);
                System.out.println(examSchedule);
                if (booking != null && booking.getStatus().equals("Pending")) {
                    // Save the new ExamSchedule
                    System.out.println(scheduleDAO.hasDuplicateUserInRoom(booking.getUserID(), examSchedule.getSlotID(), examSchedule.getDate()));;
                    if (!scheduleDAO.hasDuplicateUserInRoom(booking.getUserID(), examSchedule.getSlotID(), examSchedule.getDate())) {
                        boolean isScheduled = scheduleDAO.updateExamScheduleUserID(
                                booking.getUserID(),
                                examSchedule.getExamID(),
                                examSchedule.getRoomID(),
                                examSchedule.getSlotID(),
                                examSchedule.getDate());

                        if (isScheduled) {
                            // Update the status of the approved booking to "Approve"
                            boolean isApproved = bookingDAO.approveBooking(bookingId);

                            if (isApproved) {
                                // Update the status of other user's bookings for the same exam schedule to "Cancel"
                                bookingDAO.cancelOtherBookings(booking.getExamScheduleID());

                                response.sendRedirect("teacher-request?msg=Booking approved and scheduled successfully!!");
                            }
                        }
                    } else {
                        response.sendRedirect("teacher-request?err=This teacher will busy at this day!!");
                    }

                }
            }
        } else {
            response.sendRedirect("login?err=You must login first!");
        }
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
