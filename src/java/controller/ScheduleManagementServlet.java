/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */
package controller;

import dao.*;
import jakarta.servlet.RequestDispatcher;
import java.io.IOException;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.util.List;
import model.Exam;
import model.ExamSchedule;
import model.Room;
import dao.ScheduleDAO;
import model.Slot;
import model.User;

/**
 *
 * @author ADMIN
 */
@WebServlet(name = "ScheduleManagementServlet", urlPatterns = {"/schedule-management"})

public class ScheduleManagementServlet extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        HttpSession session = request.getSession();
        User account = (User) session.getAttribute("account");
        if (account != null) {
            ScheduleDAO scheduleDAO = new ScheduleDAO();
            String pageParam = request.getParameter("page");
            String searchParam = request.getParameter("search");
            int page = 1; // Default to the first page
            int pageSize = 6; // Set the desired page size
            if (pageParam != null && !pageParam.isEmpty()) {
                // Parse the page parameter to an integer
                page = Integer.parseInt(pageParam);
            }
            List<ExamSchedule> examSchedules = scheduleDAO.getAllExamSchedules(searchParam);
            List<ExamSchedule> pagingExams = scheduleDAO.Paging(examSchedules, page, pageSize);

            ExamDAO examDAO = new ExamDAO();
            List<Exam> exams = examDAO.getAllExams();

            RoomDAO roomDAO = new RoomDAO();
            List<Room> rooms = roomDAO.getAllRooms();

            SlotDAO slotDAO = new SlotDAO();
            List<Slot> slots = slotDAO.getAllSlots();

            request.setAttribute("searchParam", searchParam);
            request.setAttribute("listS", pagingExams);
            request.setAttribute("exams", exams);
            request.setAttribute("rooms", rooms);
            request.setAttribute("slots", slots);
            request.setAttribute("totalPages", examSchedules.size() % pageSize == 0 ? (examSchedules.size() / pageSize) : (examSchedules.size() / pageSize + 1));
            request.setAttribute("currentPage", page);
            RequestDispatcher dispatcher = request.getRequestDispatcher("/schedule-management.jsp");
            dispatcher.forward(request, response);
        } else {
            response.sendRedirect("login?err=You must login first!");
        }

    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String action = request.getParameter("action");
        HttpSession session = request.getSession();
        User account = (User) session.getAttribute("account");
        ScheduleDAO scheduleDAO = new ScheduleDAO();
        if (account != null) {
            switch (action) {
                case "add": {
                    int examId = Integer.parseInt(request.getParameter("examId"));
                    int roomId = Integer.parseInt(request.getParameter("roomId"));
                    int slotId = Integer.parseInt(request.getParameter("slotId"));
                    String dateString = request.getParameter("examDate");
                    // Convert the date string to a Date object
                    java.util.Date utilDate = null;
                    try {
                        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
                        utilDate = dateFormat.parse(dateString);
                    } catch (ParseException e) {
                        e.printStackTrace(); // Handle the parsing exception as needed
                    }       // Convert java.util.Date to java.sql.Date
                    java.sql.Date sqlDate = new java.sql.Date(utilDate.getTime());
                    if (sqlDate.toLocalDate().isBefore(LocalDate.now())) {
                        response.sendRedirect("schedule-management?err=Cannot select dates in the past!");
                        break;
                    }
                    boolean isDuplicate = scheduleDAO.isDuplicateSchedule(roomId, slotId, sqlDate.toLocalDate());
                    if (isDuplicate) {
                        response.sendRedirect("schedule-management?err=Add failed: In " + sqlDate + " this room had been booked!");
                    } else {
                        boolean isAdded = scheduleDAO.addExamSchedule(examId, roomId, slotId, sqlDate);
                        if (isAdded) {
                            response.sendRedirect("schedule-management?msg=Schedule add successfully");
                        }
                    }
                    break;
                }
                case "edit": {
                    // Get parameters for editing an exam schedule
                    int scheduleId = Integer.parseInt(request.getParameter("id"));
                    int examId = Integer.parseInt(request.getParameter("examId"));
                    int roomId = Integer.parseInt(request.getParameter("roomId"));
                    int slotId = Integer.parseInt(request.getParameter("slotId"));
                    LocalDate examDate = LocalDate.parse(request.getParameter("examDate"));
                    // Now you have the parameters needed for updating the exam schedule
                    // Create an ExamSchedule object and set the values
                    ExamSchedule updatedSchedule = new ExamSchedule();
                    updatedSchedule.setScheduleID(scheduleId);
                    updatedSchedule.setExamID(examId);
                    updatedSchedule.setRoomID(roomId);
                    updatedSchedule.setSlotID(slotId);
                    updatedSchedule.setDate(examDate);
                    boolean isDuplicate = scheduleDAO.isDuplicateSchedule(roomId, slotId, examDate);
                    if (isDuplicate) {
                        response.sendRedirect("schedule-management?err=Update failed: In " + examDate + " this room had been booked!");
                    } else {
                        boolean isUpdated = scheduleDAO.updateExamSchedule(updatedSchedule);

                        if (isUpdated) {
                            response.sendRedirect("schedule-management?msg=Update successfully");
                        } else {
                            response.sendRedirect("schedule-management?err=Update failed!");
                        }
                    }
                    break;
                }
                case "delete":
                    int id = Integer.parseInt(request.getParameter("id"));
                    boolean isDeleted = scheduleDAO.deleteExamSchedule(id);
                    if (isDeleted) {
                        response.sendRedirect("schedule-management?msg=Delete successfully");
                    } else {
                        response.sendRedirect("schedule-management?err=Delete failed!");
                    }
                    break;
                default:
                    break;
            }
        } else {
            response.sendRedirect("login?err=You must login first!");
        }
    }

}
