/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */
package controller;

import dao.BookingDAO;
import dao.ScheduleDAO;
import jakarta.servlet.RequestDispatcher;
import java.io.IOException;
import java.io.PrintWriter;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;
import java.time.LocalDate;
import java.util.List;
import model.Booking;
import model.ExamSchedule;
import model.User;

/**
 *
 * @author ADMIN
 */
// của teacher
@WebServlet(name = "ExamBookingServlet", urlPatterns = {"/exam-booking"})
public class ExamBookingTeacherServlet extends HttpServlet {

    @Override
  protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
    HttpSession session = request.getSession();
    User account = (User) session.getAttribute("account");
    if (account != null) {
        ScheduleDAO scheduleDAO = new ScheduleDAO();
        BookingDAO bookingDAO = new BookingDAO();
        String pageParam = request.getParameter("page");
        String searchParam = request.getParameter("search");
        int page = 1; // Default to the first page
        int pageSize = 6; // Set the desired page size
        if (pageParam != null && !pageParam.isEmpty()) {
            // Parse the page parameter to an integer
            page = Integer.parseInt(pageParam);
        }

        // lấy all exam schedules mà chưa assigned
        List<ExamSchedule> examSchedules = scheduleDAO.getAllExamSchedulesNotAssigned(searchParam);
        
        // Check for duplicate bookings for each exam schedule
        for (ExamSchedule examSchedule : examSchedules) {
            boolean isDuplicate = bookingDAO.isDuplicateBooking(account.getId(), examSchedule.getScheduleID());           
            // Set an attribute indicating whether a duplicate booking exists for this exam schedule
            request.setAttribute("duplicateBooking_" + examSchedule.getScheduleID(), isDuplicate);
        }
       
        List<ExamSchedule> pagingExams = scheduleDAO.Paging(examSchedules, page, pageSize);

        request.setAttribute("searchParam", searchParam);
        request.setAttribute("listS", pagingExams);
        request.setAttribute("totalPages", examSchedules.size() % pageSize == 0 ? (examSchedules.size() / pageSize) : (examSchedules.size() / pageSize + 1));
        request.setAttribute("currentPage", page);
        request.getRequestDispatcher("/booking.jsp").forward(request, response);
       
    } else {
        response.sendRedirect("login?err=You must login first!");
    }
}
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        HttpSession session = request.getSession();
        BookingDAO dao = new BookingDAO();
        User account = (User) session.getAttribute("account");

        if (account != null) {
            try {
                int scheduleID = Integer.parseInt(request.getParameter("id"));

                // Use LocalDate.now() to get the current date
                LocalDate currentDate = LocalDate.now();

                // Create a Booking object with the necessary information
                Booking booking = new Booking(account.getId(), scheduleID, currentDate, "Pending");

                // Add the booking request
                boolean isAdded = dao.addBookingRequest(booking);

                if (isAdded) {
                    response.sendRedirect("teacher-request?msg=Booking request added successfully");
                } else {
                    response.sendRedirect("exam-booking?err=You already this exam schedule! ");
                }
            } catch (NumberFormatException e) {
                e.printStackTrace(); // Handle or log the exception as needed
                response.sendRedirect("booking-management?err=Invalid schedule ID");
            }
        } else {
            response.sendRedirect("login?err=You must login first!");
        }
    }

}
