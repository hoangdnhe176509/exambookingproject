/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */
package controller;

import dao.ExamDAO;
import dao.UserDAO;
import java.io.IOException;
import java.io.PrintWriter;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;
import java.time.DayOfWeek;
import java.time.LocalDate;
import java.time.temporal.TemporalAdjusters;
import java.util.List;
import model.ExamSchedule;
import dao.ScheduleDAO;
import model.User;

/**
 *
 * @author ADMIN
 */
@WebServlet(name = "TeacherExamScheduleServlet", urlPatterns = {"/teacher-view-schedule"})
public class TeacherExamScheduleServlet extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        HttpSession session = request.getSession();
        User account = (User) session.getAttribute("account");
        if (account != null) {
            String startDateStr = request.getParameter("startDate");
            LocalDate startDate = startDateStr != null ? LocalDate.parse(startDateStr) : LocalDate.now().with(TemporalAdjusters.previousOrSame(DayOfWeek.MONDAY));
            LocalDate endDate = startDate.plusDays(6);
            ScheduleDAO examDAO = new ScheduleDAO();
            List<ExamSchedule> schedule = examDAO.getExamSchedulesByUserID(account.getId(), startDate, endDate);
            request.setAttribute("schedule", schedule);
            request.setAttribute("startDate", startDate);
            request.setAttribute("endDate", endDate);
            request.getRequestDispatcher("teacher-exam-schedule.jsp").forward(request, response);
        } else {
            response.sendRedirect("login?err=You must login first!");
        }

    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
    }

}
