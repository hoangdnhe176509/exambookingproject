/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */
package controller;

import dao.ExamDAO;
import jakarta.servlet.RequestDispatcher;
import java.io.IOException;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;
import java.util.ArrayList;
import java.util.List;
import model.Exam;
import model.User;

/**
 *
 * @author ADMIN
 */
@WebServlet(name = "ExamMangement", urlPatterns = {"/exam-management"})
public class ExamMangement extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        HttpSession session = request.getSession();
        User account = (User) session.getAttribute("account");
        if (account != null) {
            ExamDAO examDAO = new ExamDAO();
            String pageParam = request.getParameter("page");
            String searchParam = request.getParameter("search");
            int page = 1; // Default to the first page
            int pageSize = 6; // Set the desired page size
            if (pageParam != null && !pageParam.isEmpty()) {
                // Parse the page parameter to an integer
                page = Integer.parseInt(pageParam);
            }
            // sreach exam
            List<Exam> subject = examDAO.getAllExams();
            List<Exam> exams = examDAO.getAllExamWithParam(searchParam);
            List<Exam> pagingExams = examDAO.Paging(exams, page, pageSize);
            request.setAttribute("listS", subject);
            request.setAttribute("searchParam", searchParam);
            request.setAttribute("listE", pagingExams);
            request.setAttribute("totalPages", exams.size() % pageSize == 0 ? (exams.size() / pageSize) : (exams.size() / pageSize + 1));
            request.setAttribute("currentPage", page);
            request.getRequestDispatcher("/exam-management.jsp").forward(request, response);

        } else {
            response.sendRedirect("login?err=You must login first!");
        }

    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String action = request.getParameter("action");
        ExamDAO examDAO = new ExamDAO();
        if ("delete".equals(action)) {
            int id = Integer.parseInt(request.getParameter("id"));
            request.getParameter("id");
            boolean isDelete = examDAO.deleteExam(id);
            if (isDelete) {
                response.sendRedirect("exam-management?msg=Delete exam successfully!");
            } else {
                response.sendRedirect("exam-management?err=Failed to delete exame!");
            }
        } else if ("add".equals(action)) {
            String name = request.getParameter("name");
            String subject = request.getParameter("subject");
            
            
            
            Exam e = new Exam(name, subject);
            boolean isAdded = examDAO.addExam(e);
            if (isAdded) {
                response.sendRedirect("exam-management?msg=Add exam successfully!");
            } else {
                response.sendRedirect("exam-management?err=Failed to add exam!");
            }
            
        } else if ("edit".equals(action)) {
            int id = Integer.parseInt(request.getParameter("id"));
            String name = request.getParameter("name");
            String subject = request.getParameter("subject");
            Exam e = new Exam(id, name, subject);
            boolean isEdited = examDAO.editExam(e);
            if (isEdited) {
                response.sendRedirect("exam-management?msg=Edit exam successfully!");
            } else {
                response.sendRedirect("exam-management?err=Failed to edit exam!");
            }
        }
    }
}
