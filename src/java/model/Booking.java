/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package model;

import java.sql.Date;
import java.time.LocalDate;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

/**
 *
 * @author ADMIN
 */
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class Booking {
    private int id;
    private int userID;
    private int examScheduleID;
    private LocalDate date;
    private String status;
    
    private String userName;
    private String slotName;
    private String roomName;
    private String examName;
    private LocalDate examDate;

    public Booking(int userID, int examScheduleID, LocalDate date, String status) {
        this.userID = userID;
        this.examScheduleID = examScheduleID;
        this.date = date;
        this.status = status;
    }
    
}
