/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package model;

import java.time.LocalDate;
import java.time.LocalTime;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

/**
 *
 * @author ADMIN
 */
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class ExamSchedule {
    private int scheduleID;
    private int userID;
    private int examID;
    private int roomID;
    private int slotID;
    private LocalDate date;

    // Additional details for display
    private String firstName;
    private String lastName;
    private String examName;
    private String subject;
    private String roomName;
    private String slotName;
    private LocalTime startTime;
    private LocalTime endTime;
}
